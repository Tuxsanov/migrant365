const user = (state = [], action) => {
  switch (action.type) {
    case 'ADD_USER':
      return action.data
    case 'UPDATE_USER':
      return {
        ...state,
        ...action.data
      }
    case 'REMOVE_USER':
      return {};
    default:
      return state
  }
}

export default user
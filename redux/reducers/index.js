import { combineReducers } from 'redux'
import countries from './countries'
import origins from './origins'
import nations from './nations'
import user from './user'
import form from './form'
import forumSelected from './forum-selected'
import visibilityFilter from './visibilityFilter'

export default combineReducers({
  countries,
  origins,
  nations,
  user,
  forumSelected,
  form,
  //-------- 
  visibilityFilter,
})
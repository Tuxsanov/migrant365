const origins = (state = [], action) => {
    switch (action.type) {
      case 'ADD_ORIGIN':
        return [
          ...state,
          action.data
        ]
      case 'REMOVE_ORIGIN':
        return state.filter(origin=>origin.id !== action.data.id);
      case 'RESET_ORIGIN':
        return [];
      default:
        return state
    }
}

export default origins
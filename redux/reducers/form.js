const form = (state = {}, action) => {
  switch (action.type) {
    case 'SET_FORM':
      return action.data
    case 'UPDATE_FORM':
      return {
        ...state,
        ...action.data
      }
    case 'REMOVE_FORM':
      return {};
    default:
      return state
  }
}

export default form
const countries = (state = [], action) => {
    switch (action.type) {
      case 'ADD_COUNTRY':
        return [
          ...state,
          action.data
        ]
      case 'REMOVE_COUNTRY':
        return state.filter(country=>country.id !== action.data.id);
      case 'TOGGLE_TODO':
        return state.map(todo =>
          todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
        )
      default:
        return state
    }
}

export default countries
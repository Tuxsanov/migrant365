const nations = (state = [], action) => {
    switch (action.type) {
      case 'ADD_NATION':
        return [
          ...state,
          action.data
        ]
      case 'REMOVE_NATION':
        return state.filter(nation=>nation.id !== action.data.id);
      default:
        return state
    }
}

export default nations
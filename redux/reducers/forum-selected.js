const forumSelected = (state = {}, action) => {
    switch (action.type) {
      case 'SELECTED_FORUM':
        return action.data
      case 'REMOVE_SELECTED_FORUM':
        return {}
      default:
        return state
    }
}

export default forumSelected
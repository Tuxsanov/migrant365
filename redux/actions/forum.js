// ACTIONS FORUM
export const selectForum = data => ({
    type: 'SELECTED_FORUM',
    data
});

export const removeSelectedForum = data => ({
    type: 'REMOVE_SELECTED_FORUM',
    data
});
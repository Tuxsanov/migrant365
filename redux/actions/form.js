// USER: (action)

export const setForm = data => ({
    type: 'SET_FORM',
    data
});

export const updateForm = data => ({
    type: 'UPDATE_FORM',
    data
});

export const removeForm = data => ({
    type: 'REMOVE_FORM',
    data
});

export const addCountries = data => ({
  type: 'ADD_COUNTRY',
  data
});

export const removeCountry = data => ({
  type: 'REMOVE_COUNTRY',
  data
})

export const addNation = data => ({
  type: 'ADD_NATION',
  data
});

export const removeNation = data => ({
  type: 'REMOVE_NATION',
  data
})

export const addOrigin = data => ({
  type: 'ADD_ORIGIN',
  data
});

export const removeOrigin = data => ({
  type: 'REMOVE_ORIGIN',
  data
});

export const resetOrigin = () => ({
  type: 'RESET_ORIGIN',
});

export const setVisibilityFilter = filter => ({
  type: 'SET_VISIBILITY_FILTER',
  filter
})

export const toggleTodo = id => ({
  type: 'TOGGLE_TODO',
  id
})

export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_COMPLETED: 'SHOW_COMPLETED',
  SHOW_ACTIVE: 'SHOW_ACTIVE'
}

// USER: (action)

export const addUser = data => ({
    type: 'ADD_USER',
    data
});

export const updateUser = data => ({
    type: 'UPDATE_USER',
    data
});

export const removeUser = data => ({
    type: 'REMOVE_USER',
    data
});

import React from "react";
import ScrollableTabView, {DefaultTabBar, ScrollableTabBar } from 'react-native-scrollable-tab-view';
import { 
  StyleSheet,
  ScrollView,
  Image, View, Text } from "react-native";
import { 
  createBottomTabNavigator,
  createAppContainer 
} from "react-navigation";
import { material, robotoWeights } from 'react-native-typography';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FacebookTabBar from './fb';
import { Avatar, Appbar, Surface, Divider, Button } from 'react-native-paper';


class HomeScreen extends React.Component {
  _goBack = () => console.log('Went back');

  _onSearch = () => console.log('Searching');

  _onMore = () => console.log('Shown more');

  render() {
    return (
      <View style={{flex: 1}}>
        <Surface style={{width: '100%', flexDirection: 'column',}}>
          <View style={{height: 58, justifyContent: 'center'}}>
            <Image style={{width: 134, height: 43, marginHorizontal: 15,}} source={require('./imgs/logo/toolbar.png')} />
          </View>
        </Surface>

        <ScrollableTabView
          initialPage={1}
          renderTabBar={() => <ScrollableTabBar />}
        >
          <ScrollView tabLabel="Uzbek" style={styles.tabView}>
            <View style={styles.card}>
              <Text>News</Text>
            </View>
          </ScrollView>
          <ScrollView tabLabel="Turkish" style={{flex: 1,}}>
            <View style={{paddingHorizontal:15, paddingVertical: 10, alignItems: 'center', flexDirection:'row'}}>
              <Text style={material.button}>Tadbirlar / 2 Manzil</Text>
              <View style={{marginLeft: 'auto', width:40, height:40,padding: 0, borderRadius: 40,backgroundColor: "#000", justifyContent:'center', alignItems: 'center'}}>
                <MaterialIcons color="#fff" size={22} name="filter-list" />
              </View>
            </View>
            <Surface style={{
              paddingHorizontal: 15, 
              paddingVertical: 10,
              borderWidth: 1,
              borderLeftWidth: 0,
              borderRightWidth: 0,
              borderColor: 'rgba(0,0,0,.1)',
            }}>
              <View style={{flexDirection: 'row',}}>
                <Text style={material.caption}>#Russia > Moscow #passport</Text>
                <MaterialIcons style={{marginLeft: 'auto'}} color="#000" size={12} name="flag" />
              </View>
              <Text style={material.title}>
                Moskva Tashkent 1500 rubldan odam soniga
              </Text>
              <View style={{flexDirection: 'row',  paddingVertical: 5,}}>
                <Text style={{...material.caption, ...robotoWeights.bold, marginRight: 5,}}>12 comments</Text>
                <Text style={{...material.caption, ...robotoWeights.bold, marginRight: 5,}}>/</Text>
                <Text style={{...material.caption, ...robotoWeights.bold, marginRight: 5,}}>12 applicants</Text>
                <Text style={{...material.caption, ...robotoWeights.bold, marginRight: 5,}}>/</Text>
                <View style={{flexDirection: 'row'}}>
                  <MaterialIcons style={{marginLeft: 'auto'}} color="orange" size={12} name="star" />
                  <Text style={{...material.caption, ...robotoWeights.bold, marginLeft: 2,}}>3.7 rates</Text>
                </View>
              </View>
              <View style={{marginBottom: 5}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Avatar.Text size={30} label="XD" />
                  <View style={{marginLeft: 10, marginRight: 'auto'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Text style={{...material.body2, marginVertical: 0}}>Rebeth Duer</Text>
                      <MaterialIcons style={{marginLeft: 5}} color="orange" size={12} name="check-box" />
                    </View>
                    <Text style={{...material.caption, marginBottom: 0,}}>Uzbekistan</Text>
                  </View>
                  <Button mode="outlined" onPress={() => console.log('Pressed')}>
                    Apply
                  </Button>
                </View>
              </View>
              <Text style={{...material.caption, fontSize: 10}}>12 Oct 2018  12:00 pm</Text>
            </Surface>
          </ScrollView>
          <ScrollView tabLabel="Spanish" style={styles.tabView}>
            <View style={styles.card}>
              <Text>Messenger</Text>
            </View>
          </ScrollView>
          <ScrollView tabLabel="Russian" style={styles.tabView}>
            <View style={styles.card}>
              <Text>Notifications</Text>
            </View>
          </ScrollView>
          
          <ScrollView tabLabel="Arabian" style={styles.tabView}>
            <View style={styles.card}>
              <Text>Notifications</Text>
            </View>
          </ScrollView>
        </ScrollableTabView>
        
      </View>
    );
  }
}

class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }
}

const TabNavigator = createBottomTabNavigator({
  Home: HomeScreen,
  Notifications: HomeScreen,
  Add: {
    screen: HomeScreen,
    navigationOptions: {
      tabBarLabel: ()=>{}
    }
  },
  Messages: HomeScreen,
  Me: SettingsScreen,
},{
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      let IconComponent = MaterialIcons;
      let iconName;
      
      if (routeName === 'Home') {
        iconName = `place`;
        IconComponent = HomeIconWithBadge; 
      } else if (routeName === 'Notifications') {
        iconName = `notifications`;
      } else if (routeName === 'Add') {
        // iconName = `account-circle`;
        IconComponent = AddBtn;
      } else if (routeName === 'Messages') {
        iconName = `chat-bubble`;
      } else if (routeName === 'Me') {
        iconName = `account-circle`;
      }

      // You can return any component that you like here!
      return <IconComponent name={iconName} size={25} color={tintColor} />;
    },
  }),
  tabBarOptions: {
    activeTintColor: '#000',
    inactiveTintColor: 'rgba(0,0,0,0.5)',
  }
});

const AddBtn = () =>{
  return (
    <View style={{ 
      borderRadius: 50,
      backgroundColor: '#000',
      justifyContent: 'center',
      alignItems: 'center',
      width: 40, height: 40, }}>
      <MaterialIcons color="red" size={22} name="add" />
      {/* size={size} color={color} */}
    </View>
  )
}

const HomeIconWithBadge = (props) => {
  // You should pass down the badgeCount in some other ways like react context api, redux, mobx or event emitters.
  return <IconWithBadge {...props} badgeCount={3} />;  
}

class IconWithBadge extends React.Component {
  render() {
    const { name, badgeCount, color, size } = this.props;
    return (
      <View style={{ width: 24, height: 24, margin: 5 }}>
        <MaterialIcons name={name} size={size} color={color} />
        { badgeCount > 0 && (
          <View style={{
            // If you're using react-native < 0.57 overflow outside of the parent
            // will not work on Android, see https://git.io/fhLJ8
            position: 'absolute',
            right: -6,
            top: -3,
            backgroundColor: 'red',
            borderRadius: 6,
            width: 12,
            height: 12,
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>{badgeCount}</Text>
          </View>
        )}
      </View>
    );
  }
}




const styles = StyleSheet.create({
  tabView: {
    flex: 1,
    padding: 10,
    paddingHorizontal: 15,
    backgroundColor: 'rgba(0,0,0,0.01)',
  },
  card: {
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: 'rgba(0,0,0,0.1)',
    margin: 5,
    height: 150,
    padding: 15,
    shadowColor: '#ccc',
    shadowOffset: { width: 2, height: 2, },
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  surface: {
    padding: 8,
    height: 80,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 4,
  },
});

export default createAppContainer(TabNavigator);
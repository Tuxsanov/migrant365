import React, { Component } from 'react';
import { FlatList, View, Text, ScrollView } from 'react-native';
import { Appbar, Colors, List, TextInput, Divider, Button, Snackbar, ActivityIndicator, Card, TouchableRipple, Title, Caption, IconButton, Paragraph, Banner, Chip, Subheading, Checkbox, FAB } from 'react-native-paper';

// Localize
import * as RNLocalize from "react-native-localize";
import I18n from "../../uitils/I18n";
const {countryCode, languageCode} = RNLocalize.getLocales()[0];

// Firebase
import firebase from 'react-native-firebase';
import AsyncStorage, { useAsyncStorage } from '@react-native-community/async-storage';

// Navigation
import { NavigationActions, StackActions } from 'react-navigation';
import { material, robotoWeights } from 'react-native-typography';
import { keyExtractor } from '../home/functions/key-extractor';
import { changeRoute } from '../../functions/route';
import LocationNationStepsComponent from './steps-check';

import { connect } from 'react-redux';
import { addCountries, removeCountry, resetOrigin, addNation, removeNation, addUser, updateCountryCityNation, selectCity } from '../../../redux/actions';

// Lodash
import { orderBy, findIndex } from 'lodash';
import MGToolbarBack from '../../components/toolbar-back';
import { translate } from '../../functions/translate';
import { updateUser } from '../../../redux/actions/user';

class NationSelectScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            nations: []
        };
    }

    componentDidMount(){
        firebase.firestore().collection('nationality').get().then(snapshot=>{
            let nations = [];
            snapshot.forEach(e=>{
                nations.push({
                    id: e.id,
                    ...e.data()
                });
            });
            this.setState({nations, loading: false});
        });
    }

    selectNation = (nation)=>{
        let nations = this.state.nations;
        const _index = findIndex(nations, e=>e.id == nation.id);
        nations[_index].checked = !nations[_index].checked;
        if(nations[_index].checked){
            if(this.props.user.nations.length<2){
                this.props.updateUser({nations});
                // this.props.addNation(nation);
                this.setState({nations});
            }else{
                alert('+++')
                // nations[_index].checked = !nations[_index].checked;
                // this.setState({nationsDialog:JSON.parse(JSON.stringify(this.props.selectedNations)), errSnackbar: true})
            }
        }else{
            this.props.removeNation(nation);
        }
    }

    save = ()=>{
        this.setState({loading: true});
        // const user = await AsyncStorage.getItem('user');
        // const {name, lastname, email, age, gender} = this.state;
        // alert(this.state.user.uid)
        firebase.firestore().collection('user').doc(this.state.user.uid)
        .update({
            countries: this.props.selectedCountries,
            origins: this.props.selectedOrigins,
            nations: this.props.selectedNations,
            selectedCity: this.props.selectedOrigins[0].id,
        }).then(e=>{
            this.setState({loading: false})
            this.props.updateCountryCityNation({
                countries: this.props.selectedCountries,
                origins: this.props.selectedOrigins,
                nations: this.props.selectedNations
            });

            this.props.selectCity(this.props.selectedOrigins[0].id);


            const resetAction = StackActions.reset({
                index: 1,
                actions: [
                    NavigationActions.navigate({ routeName: 'Home' }),
                    NavigationActions.navigate({ routeName: 'Profile' })
                ],
            });
            this.props.navigation.dispatch(resetAction);

            // changeRoute(this.props.navigation, 'Profile', 'reset');
        }).catch(err=>{
            alert(err)
        })
    }

    renderItem = ({item, index})=>(
        <MyListItem
            item={item}
            checked={item.checked}
            selectNation={this.selectNation}
        />
    );

    render() {
        const {nations, loading} = this.state;
        return (
            <View style={{flex:1}}>
                <MGToolbarBack title={translate('nationSelect.title')} navigation={this.props.navigation} />

                <FlatList
                    onRefresh={()=>console.log('--- ')}
                    refreshing={loading}
                    style={{backgroundColor: Colors.grey200,}}
                    onEndReachedThreshold={20}

                    initialNumToRender={20}
                    maxToRenderPerBatch={20}
                    windowSize={20}

                    ItemSeparatorComponent={()=><Divider />}

                    data={nations}
                    renderItem={this.renderItem}
                    keyExtractor={keyExtractor}
                />
            </View>
        );
    }
}


const mapDispatchToProps = dispatch => {
    return {
        updateUser: (user)=>dispatch(updateUser(user)),

        addUser: (nation)=>dispatch(addUser(nation)),
        addNation: (nation)=>dispatch(addNation(nation)),
        removeNation: (nation)=>dispatch(removeNation(nation)),

        updateCountryCityNation: (nation)=>dispatch(updateCountryCityNation(nation)),
        selectCity: (city)=>dispatch(selectCity(city)),

        resetOrigin: ()=>dispatch(resetOrigin()),
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,

        selectedNations: state.nations,

        selectedOrigins: state.origins,

        selectedCountries: state.countries
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NationSelectScreen);

class MyListItem extends React.PureComponent {
    render() {
        const {item, selectNation} = this.props;
        return (
            <List.Item
                style={{backgroundColor: Colors.white}}
                title={item.nationality_en}
                onPress={()=>selectNation(item)}
                right={()=>(
                    <Checkbox
                        status={item.checked ? 'checked' : 'unchecked'}
                        onPress={()=>selectNation(item)}
                    />
                )}
            />
        )
    }
}

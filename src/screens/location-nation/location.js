import React, { Component } from 'react';
import { Image, FlatList, View, Text, ScrollView } from 'react-native';
import { Appbar, Colors, List, Subheading, TextInput, Divider, Badge, Button, Snackbar, ActivityIndicator, Card, TouchableRipple, Title, Caption, IconButton, DataTable, Avatar, Checkbox, Dialog, Paragraph, Portal, FAB, Searchbar, Headline } from 'react-native-paper';

// Localize
import * as RNLocalize from "react-native-localize";
import I18n from "../../uitils/I18n";
const {countryCode, languageCode} = RNLocalize.getLocales()[0];

// Firebase
import firebase from 'react-native-firebase';
import AsyncStorage, { useAsyncStorage } from '@react-native-community/async-storage';

// Navigation
import { robotoWeights, material } from 'react-native-typography';
import { keyExtractor } from '../home/functions/key-extractor';
import { changeRoute } from '../../functions/route';

// Icons
import MGToolbarBack from '../../components/toolbar-back';
import LocationNationStepsComponent from './steps-check';

// Lodash
import { orderBy, findIndex } from 'lodash';

// Redux
import { connect } from 'react-redux';
import { addCountries, removeCountry, resetOrigin } from '../../../redux/actions';
import { NavigationActions } from 'react-navigation';
import { translate } from '../../functions/translate';
import { updateUser } from '../../../redux/actions/user';

class LocationSelectScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            dialogOrigins: false,
            errSnackbar: false,
            countries: [],
            regions: [],
            countriesDialog: [],
        };
    }
    
    async componentDidMount(){
        const countries = await firebase.firestore()
            .collection('country')
            .orderBy('name_en')
            .get();

        let countryList = [];
        countries.forEach((e)=>{
            countryList.push({
                id: e.id,
                ...e.data()
            });
        });

        // if(this.props.user.countryList.length>0){
        //     this.props.user.countryList.map((country)=>{
        //         const index = findIndex(countryList, (e)=>e.id == country.id);
        //         countryList[index].checked = true;
        //     })
        // }

        // countryList = orderBy(countryList, 'checked', 'asc');

        this.setState({countries: countryList, loading: false});
    }

    componentWillUnmount(){
        this.props.updateUser({countryList: this.state.countries.filter((country)=>country.checked)});
    }

    selectCountry = (country)=>{
        country.checked = !country.checked;
        // this.setState({countries: this.state.countries});
        this.save(country);
    }

    save = (country)=>{
        this.setState({loading: true});
        // const userCountries = firebase.firestore()
        // .collection('user').doc(this.props.user.uid)
        // .collection('countries')
        // .doc(country.id);

        // if(country.checked){
        //     userCountries.set({
        //         country,
        //     },{merge: true}).then().catch(err=>alert(err));
        // }else{
        //     userCountries.delete().then().catch(err=>alert(err));;
        // }

        this.props.updateUser({selectedCountry: country.id, selectedCountryName: country.name_ru});
        firebase.firestore().collection('user').doc(this.props.user.uid)
        .set({
            selectedCountry: country.id, selectedCountryName: country.name_ru
        }, {
            merge: true
        }).then(()=>{
            this.setState({
                loading: false
            });
            this.props.navigation.dispatch(NavigationActions.back());
        }).catch(err=>alert(err));
    }

    _hideDialog = () => this.setState({ dialogOrigins: false });

    renderItem = ({item, index})=>(
        <RenderItemPure
            item={item}
            checked={item.checked}
            selectCountry={this.selectCountry}
        />
    );

    render() {
        const {countries, loading} = this.state;
        return (
            <View style={{flex:1, backgroundColor: Colors.grey200,}}>
                <MGToolbarBack title={translate('locationSelectScreen.title')} navigation={this.props.navigation} />
                <FlatList
                    onRefresh={()=>console.log('--- ')}
                    refreshing={loading}
                    initialNumToRender={30}
                    extraData={false}
                    maxToRenderPerBatch={20}
                    windowSize={10}

                    ItemSeparatorComponent={()=><Divider />}

                    data={countries}
                    renderItem={this.renderItem}
                    keyExtractor={keyExtractor}
                />

            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateUser: (user)=>dispatch(updateUser(user)),
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LocationSelectScreen);

class RenderItemPure extends React.PureComponent {
    render() {
        const {item, checked, selectCountry} = this.props;
        return (
            <List.Item
                style={{backgroundColor: Colors.white}}
                onPress={()=>selectCountry(item)}
                title={item['name_' + 'ru']}
                left={() => (
                    <Avatar.Image size={35} style={{alignSelf: 'center',}} source={{uri: `https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${item.id}.png`}} />
                )}
                right={()=>
                    <IconButton
                        style={{marginLeft: 'auto'}}
                        icon="check-circle"
                        color={checked?Colors.red500:Colors.grey400}
                        size={24}
                        onPress={()=>selectCountry(item)}
                    />
                }
            />
        )
    }
}
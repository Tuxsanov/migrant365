import React, { Component } from 'react';
import { FlatList, View, Text } from 'react-native';
import { Colors, List, Checkbox, Portal, Dialog, Button } from 'react-native-paper';

// Firebase
import firebase from 'react-native-firebase';
import AsyncStorage, { useAsyncStorage } from '@react-native-community/async-storage';
import { OriginRender } from './origin-render';
import { keyExtractor } from '../home/functions/key-extractor';

// Lodash
import { orderBy, findIndex } from 'lodash';

// Redux
import { connect } from 'react-redux';
import { addOrigin, removeOrigin } from '../../../redux/actions';

class OriginCitiesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            origins: [],
            // selectedCities: [],
            // selectedOrigins: [], 
            // originsDialog: []
        };
    }

    // unselectCity = (country)=>{
    //     let selectedCitiesList = this.state.selectedCities;
    //     const delIndex = selectedCitiesList.indexOf(country.id);
    //     selectedCitiesList.splice(delIndex, 1);
        
    //     let regionsList = this.state.regions;
    //     const _index = regionsList.indexOf(regions);
    //     regionsList[_index].checked = false;
        
    //     this.setState({regions: regionsList, selectedCities: selectedCitiesList})
    // }

    selectCity = (origin)=>{
        alert(JSON.stringify(this.props.selectedOrigins));        

        let origins = this.state.origins;
        const _index = findIndex(origins, e=>e.id == origin.id);
        origins[_index].checked = !origins[_index].checked;
        if(origins[_index].checked){
            if(this.props.selectedOrigins.length<1){
                this.props.addOrigin(origin);
                // let selectedOrigins = this.state.selectedOrigins;
                // selectedOrigins.push(origin);
                this.setState({origins});
            }else{
                origins[_index].checked = !origins[_index].checked;
                this.setState({errSnackbar: true})
                alert(JSON.stringify(this.props.selectedOrigins))
            }
        }else{
            // let selectedOrigins = this.state.selectedOrigins;
            // const __index = findIndex(selectedOrigins, e=>e.id == origin.id);
            // selectedOrigins = selectedOrigins.splice(__index, 1);
            // this.setState({selectedOrigins})
            this.props.removeOrigin(origin);
        }

        // this.setState({errSnackbar: false})
        // let selectedCities = this.state.selectedCities;
        // let regions = this.state.regions;
        // const _index = regions.indexOf(country);
        // if(regions[_index].checked){
        //     this.unselectCity(country);
        //     regions[_index].checked = false;
        // }else{
        //     if(selectedCities.length<2){
        //         regions[_index].checked = true;

        //         selectedCities.push(country.id);
        //         this.setState({regions, selectedCities})
        //     }else{
        //         this.setState({errSnackbar: true})
        //     }
        // }
    }

    async componentDidMount(){
        this.props.resetOrigin();
        const uuid = await AsyncStorage.getItem('uuid');
        this.setState({uuid});

        firebase.firestore()
        .collection('region')
        .doc(this.props.id)
        .get()
        .then(res=>{
            if(res.exists){
                let origins = res.data().regions.map(e=>({id:e.name.replace(/ /g,''), name: e.name}));
                this.setState({origins, loading: false})
            }
        });
    }

    renderItem = ({item, index})=>(
        <MyListItem
            item={item}
            checked={item.checked}
            selectCity={this.selectCity}
        />
    );

    render() {
        const {loading, origins} = this.state;
        return (
            <View>
                <FlatList
                    onRefresh={()=>console.log('--- ')}
                    refreshing={loading}
                    
                    onEndReachedThreshold={10}
                    initialNumToRender={10}
                    maxToRenderPerBatch={10}
                    windowSize={10}
    
                    data={origins}
                    renderItem={this.renderItem}
                    keyExtractor={keyExtractor}
                />
                <Portal>
                    <Dialog
                    visible={this.state.errSnackbar}
                    onDismiss={() => this.setState({ errSnackbar: false })}
                    >
                    <Dialog.Title>You can only able to select single city for each country</Dialog.Title>
                    <Dialog.Actions>
                        <Button onPress={() => this.setState({ errSnackbar: false })}>Ok</Button>
                    </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addOrigin: (country)=>dispatch(addOrigin(country)),
        removeOrigin: (country)=>dispatch(removeOrigin(country)),
        resetOrigin: ()=>dispatch({type: 'RESET_ORIGIN',})
    }
}

const mapStateToProps = (state) => {
    return {
        selectedOrigins: state.origins
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(OriginCitiesList);

class MyListItem extends React.Component {
    render() {
        const {item, selectCity} = this.props;
        return (
            <List.Item
                style={{backgroundColor: Colors.white}}
                title={item.name}
                onPress={()=>selectCity(item)}
                description={`${0} followers / ${0} posts`}
                right={()=>(
                    <Checkbox
                        status={item.checked ? 'checked' : 'unchecked'}
                        onPress={()=>selectCity(item)}
                    />
                )}
            />
        )
    }
}

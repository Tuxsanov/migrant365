import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Card, Avatar, Caption, Button, Colors, Divider, List, IconButton, Chip, Surface, Title, FAB, Subheading } from 'react-native-paper';
import { robotoWeights } from 'react-native-typography';
import { changeRoute } from '../../functions/route';

export default class LocationNationStepsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    next = ()=>{
        const {resetOrigin, selectedCountries, navigation, navigationScreen} = this.props;
        // Next Clicked
        changeRoute(navigation, navigationScreen, 'push', {selectedCountries});

        if(navigationScreen == 'OriginSelect'){
            resetOrigin();
        }
    }

    complete = ()=>{
        // Cancel Clicked
    }

    renderIndex(index, positionIndex, correctIndex){
        const size = 45;
        return (
            (index>=positionIndex)?
                correctIndex>=index?
                <Avatar.Icon size={size} style={{backgroundColor: index==positionIndex?Colors.black:Colors.grey200,}} icon="check" color={Colors.white} />
                :
                <Avatar.Text size={size} style={{backgroundColor: index==positionIndex?Colors.black:Colors.grey200,}} icon="check" label={positionIndex+1} />
            :
            <Avatar.Icon size={size} style={{backgroundColor: index==positionIndex?Colors.black:Colors.grey200,}} icon="check" color={Colors.white} />
            // <Avatar.Text size={size} style={{backgroundColor: index==positionIndex?Colors.black:Colors.grey200,}} icon="check" label={positionIndex+1} />
        )
    }

    render() {
        const {currentSelection, correctIndex} = this.props;
        return (
            <Surface style={{marginVertical: 10, backgroundColor: Colors.white, padding: 10, alignItems: "center", zIndex:1}}>
                {
                    this.props.selectedCountries.length>0?
                    <View>
                        <View style={{flexDirection: "row", justifyContent: "center"}}>
                            {
                                this.props.selectedCountries.map((country, index)=>(
                                    <Avatar.Image 
                                        size={56} 
                                        style={{
                                            right: (index==0&&this.props.selectedCountries.length>1)?-8:0, 
                                            left: (index==1&&this.props.selectedCountries.length>1)?-8:0, 
                                            alignSelf: 'center',
                                        }} 
                                        key={country.id}
                                        source={{uri: `https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${country.id}.png`}} 
                                    />
                                ))
                            }
                        </View>
                        <Title>
                            {
                                this.props.selectedCountries.map((country, index)=>(
                                    <Text key={country.id}> {index>0?'/':''} {country.name_en}</Text>
                                ))
                            }
                        </Title>
                    </View>
                    :
                    <Caption>
                        No Countries selected
                    </Caption>
                }
                {
                    this.props.selectedOrigins.length>0?
                        <View style={{flexWrap: "wrap", flexDirection: "row", alignItems: "center", justifyContent: "center", textAlign: "center"}}>
                            {
                                this.props.selectedOrigins.map((origin, index)=>(
                                    <View key={origin.id} style={{flexDirection: "row"}}>
                                        <Chip 
                                        style={{margin: 3}}
                                        icon={()=>(
                                            <Avatar.Image 
                                                size={16}
                                                source={{uri: `https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${origin.location}.png`}} 
                                            />
                                        )}
                                        onPress={() => console.log('Pressed')}>{origin.name}</Chip>
                                        {/* <Subheading>{origin.name}</Subheading>
                                        <Subheading style={{marginLeft: 4,}}>({origin.location}){(index+1)!==this.props.selectedOrigins.length?', ':null}</Subheading> */}
                                    </View>
                                ))
                            }
                        </View>
                    :
                    <Caption>
                        No cities selected
                    </Caption>
                }

                {
                    this.props.selectedNations.length>0?
                        <View style={{flexWrap: "wrap", flexDirection: "row", alignItems: "center", justifyContent: "center", textAlign: "center"}}>
                            {
                                this.props.selectedNations.map((nation, index)=>(
                                    <View key={nation.id} style={{flexDirection: "row"}}>
                                        <Chip 
                                        style={{margin: 3}}
                                        // icon={()=>(
                                        //     <Avatar.Image 
                                        //         size={16}
                                        //         source={{uri: `https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${origin.location}.png`}} 
                                        //     />
                                        // )}
                                        onPress={() => console.log('Pressed')}>{nation.nationality_en}</Chip>
                                        {/* <Subheading>{origin.name}</Subheading>
                                        <Subheading style={{marginLeft: 4,}}>({origin.location}){(index+1)!==this.props.selectedOrigins.length?', ':null}</Subheading> */}
                                    </View>
                                ))
                            }
                        </View>
                    :
                    <Caption>
                        No nations selected
                    </Caption>
                }
            </Surface>
            // <Card style={{elevation: 0, backgroundColor: Colors.white}}>
            //     <Card.Actions style={{justifyContent: 'space-around'}}>
            //         <View style={{padding: 10, alignItems: 'center'}}>
            //             <Avatar.Icon size={56} icon="map" />
            //             {/* {this.renderIndex(currentSelection, 0, correctIndex)} */}
            //             <Caption>COUNTRIES</Caption>
            //         </View>
            //         <View style={{padding: 10, alignItems: 'center'}}>
            //             {/* {this.renderIndex(currentSelection, 1, correctIndex)} */}
            //             <Avatar.Icon size={56} icon="location-city" style={{backgroundColor: Colors.grey200,}} />                        
            //             <Caption>CITIES</Caption>
            //         </View>
            //         <View style={{padding: 10, alignItems: 'center'}}>
            //             {/* {this.renderIndex(currentSelection, 2, correctIndex)} */}
            //             <Avatar.Icon size={56} icon="language" style={{backgroundColor: Colors.grey200,}} />
            //             <Caption>NATIONS</Caption>
            //         </View>
            //     </Card.Actions>
            //     <Card.Actions style={{justifyContent: 'center'}}>
            //         {
            //             this.props.navigationScreen?
            //                 <Button 
            //                 // style={{width: '50%'}} 
            //                 disabled={correctIndex!==currentSelection} 
            //                 icon="chevron-right" mode="contained" onPress={() => this.next()}>
            //                     Next
            //                 </Button>
            //             :
            //                 <Button 
            //                 // style={{width: '50%'}} 
            //                 color={Colors.green500} icon="check" mode="contained" onPress={() => this.complete()}>
            //                     Complete
            //                 </Button>
            //         }
            //     </Card.Actions>
            //     <Divider />
            // </Card>
        );
    }
}

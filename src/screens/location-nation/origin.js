import React, { Component } from 'react';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { FlatList, View, Text, ScrollView } from 'react-native';
import { Appbar, Colors, List, Subheading, TextInput, Divider, Button, Snackbar, ActivityIndicator, Card, TouchableRipple, Title, Caption, IconButton, DataTable, Avatar, Checkbox, Portal, Dialog, FAB } from 'react-native-paper';

// Localize
import * as RNLocalize from "react-native-localize";
import I18n from "../../uitils/I18n";

// Firebase
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

// Lodash
import { orderBy, findIndex, filter } from 'lodash';

// Scrollable Tab
import ScrollableTabBar from '../../../ScrollableTabBar';
import MGToolbarBack from '../../components/toolbar-back';
import LocationNationStepsComponent from './steps-check';
import OriginCitiesList from './origin-cities-list';

// Redux
import { connect } from 'react-redux';
import { keyExtractor } from '../home/functions/key-extractor';
import { addOrigin, removeOrigin, resetOrigin } from '../../../redux/actions';
import { changeRoute } from '../../functions/route';

class OriginSelectScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            locations: this.props.selectedCountries,
            loading: false,
        };
    }

    componentDidMount(){
        this.setState({loading: true})
        this.state.locations.map((location, index)=>{
            firebase.firestore()
            .collection('region')
            .doc(location.id)
            .get()
            .then(res=>{
                if(res.exists){
                    let origins = res.data().regions.map(e=>({id:e.name.replace(/ /g,''), name: e.name, location: location.id}));
                    this.setState({
                        [location.id]: origins, 
                    });
                }
            }).finally(()=>{
                if((index+1) == this.state.locations.length){
                    this.setState({
                        loading: false
                    });
                }
            });
        })
    }

    selectCity = (origin)=>{
        let origins = this.state[origin.location];
        const _index = findIndex(origins, e=>e.id == origin.id);
        const _selectedOrigins = filter(this.props.selectedOrigins, e=>e.location == origin.location);
        origins[_index].checked = !origins[_index].checked;
        if(origins[_index].checked){
            if(_selectedOrigins.length<1){
                this.props.addOrigin(origin);
                this.setState({[origin.location]: origins});
            }else{
                origins[_index].checked = !origins[_index].checked;
                this.setState({errSnackbar: true, errSnackbarCountryId: origin.location})
            }
        }else{
            this.props.removeOrigin(origin);
        }
    }

    renderItem = ({item, index})=>(
        <MyListItem
            item={item}
            checked={item.checked}
            selectCity={this.selectCity}
        />
    );

    render() {
        const {locations} = this.state;
        return (
            <View style={{flex:1, backgroundColor: Colors.grey200,}}>
                <MGToolbarBack navigation={this.props.navigation} title={I18n.t(`OriginSelectScreen.title`, {locale: this.state.languageCode})} />
                {/* <LocationNationStepsComponent 
                    navigationScreen={'NationSelect'}
                    navigation={this.props.navigation} 
                    currentSelection={1} 
                    correctIndex={1} 
                /> */}

                <LocationNationStepsComponent 
                    currentSelection={0}
                    navigationScreen={'NationSelect'} 
                    correctIndex={this.props.selectedCountries.length>0?0:-1}
                    selectedCountries={this.props.selectedCountries}
                    selectedOrigins={this.props.selectedOrigins}
                    navigation={this.props.navigation}
                    selectedNations={[]}
                    
                    resetOrigin={this.resetOrigin}
                />

                <ScrollableTabView
                    tabBarBackgroundColor={'#fff'}
                    onChangeTab={({i})=>console.log(i)}
                    initialPage={0}
                    renderTabBar={() => <ScrollableTabBar />}
                >
                    {
                        locations.map(location=>(
                            <FlatList
                                key={location.id} 
                                tabLabel={JSON.stringify({id: location['name_en']})}

                                onRefresh={()=>console.log('--- ')}
                                refreshing={this.state.loading}
                                
                                onEndReachedThreshold={10}
                                initialNumToRender={10}
                                maxToRenderPerBatch={10}
                                windowSize={10}
                
                                data={this.state[location.id]}
                                renderItem={this.renderItem}
                                keyExtractor={keyExtractor}
                            />
                        ))
                    }
                </ScrollableTabView>

                <Portal>
                    <Dialog
                        visible={this.state.errSnackbar}
                        onDismiss={() => this.setState({ errSnackbar: false })}
                    >
                    <Dialog.Title>You can able to select only single city each country</Dialog.Title>
                    <Dialog.Content>
                        <Text>
                            We recommend to select city where you from and where you living now or plan to live.
                        </Text>
                        {
                            this.props.selectedOrigins.filter(e=>e.location==this.state.errSnackbarCountryId).map(item=>(
                                <List.Item
                                    style={{backgroundColor: Colors.grey100,}}
                                    key={item.id}
                                    title={item['name']}
                                    onPress={()=>this.selectCountryDialog(item)}
                                    right={()=>(
                                        <Checkbox
                                            status={item.checked ? 'checked' : 'unchecked'}
                                            onPress={()=>this.selectCountryDialog(item)}
                                        />
                                    )}
                                />
                            ))
                        }
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => this.setState({ errSnackbar: false })}>Ok</Button>
                    </Dialog.Actions>
                    </Dialog>
                </Portal>

                <FAB
                    style={{position: "absolute", bottom: 25, right: 20}}
                    icon="chevron-right"
                    onPress={() => changeRoute(this.props.navigation, 'NationSelect', 'push')}
                    label="Nations"
                />
                
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addOrigin: (country)=>dispatch(addOrigin(country)),
        removeOrigin: (country)=>dispatch(removeOrigin(country)),
        // resetOrigin: ()=>dispatch(resetOrigin()),

        addCountries: (country)=>dispatch(addCountries(country)),
        removeCountry: (country)=>dispatch(removeCountry(country))
    }
}

const mapStateToProps = (state) => {
    return {
        selectedOrigins: state.origins,

        selectedCountries: state.countries
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(OriginSelectScreen);


class MyListItem extends React.PureComponent {
    render() {
        const {item, selectCity} = this.props;
        return (
            <List.Item
                style={{backgroundColor: Colors.white}}
                title={item.name}
                onPress={()=>selectCity(item)}
                description={`${0} followers / ${0} posts`}
                right={()=>(
                    <Checkbox
                        status={item.checked ? 'checked' : 'unchecked'}
                        onPress={()=>selectCity(item)}
                    />
                )}
            />
        )
    }
}

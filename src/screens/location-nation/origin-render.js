import React, { Component } from 'react';
import { List } from "react-native-paper";

export const OriginRender = ({item, index})=>(
    <List.Item
        key={item.shortCode}
        title={item.name}
        style={{backgroundColor: '#fff',}}
        description={`${item.followers || 0} followers / ${item.posts || 0} posts`}
    />
);
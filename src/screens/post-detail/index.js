import React from "react";
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { View, TextInput, FlatList } from "react-native";
import { Button, Appbar, Text, Colors, Divider, IconButton } from "react-native-paper";
import ScrollableTabBar from "../../../ScrollableTabBar";
// Tab Screens
import ChatScreen from "../chat";
import TabInfo from "./info";
import TabRate from "./rate";
import ForumComponent from "../home/component/forum";

// Firebase
import firebase from 'react-native-firebase';

// Redux
import { connect } from 'react-redux';
import { keyExtractor } from "../home/functions/key-extractor";

class PostDetailScreen extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            loading: true,
            postReplies: [],
            post: {
                date: '',
                title: '',
                description: '',
                category: '',
                city: '',
                country: '',
                nations: [],
                user: {
                    uid: '',
                    gender: '',
                    avatarImg: '',
                    name: '',
                    lastname: '',
                },
            }
        }
    }

    create = ()=>{
        const {user} = this.props;
        const {uid, gender, avatarImg, name, lastname} = user;
        let _user = {uid, gender, avatarImg, name, lastname};


        let _post = this.state.post;
        _post.user = _user;
        _post.nations = user.nations;
        _post.city = user.selectedCity;

        const fs = firebase.firestore();
        fs.collection('posts')
        .doc(this.props.forumSelected.id)
        .collection('comments')
        .add(_post)
        .then(()=>{
            this.setState({post: {
                date: '',
                title: '',
                description: '',
                category: '',
            }});
        })
        .catch(e=>alert(e))
    }

    getComments = ()=>{
        const fs = firebase.firestore();
        fs.collection('posts')
        .doc(this.props.forumSelected.id)
        .collection('comments')
        .get()
        .then((snapshot)=>{
            let postReplies = [];
            snapshot.forEach(doc=>{
                postReplies.push({...doc.data(), ...{id: doc.id}});
            });
            this.setState({postReplies, loading: false});
        })
        .catch(e=>alert(e))
    }

    componentDidMount(){
        this.getComments();
    }

    renderItem = ({item, index})=>(
        item?
        <ForumComponent forum={item} navigation={this.props.navigation} />
        :null
    );

    render() {
        return (
            <View style={{flex: 1}}>
                <Appbar.Header style={{backgroundColor: Colors.white}}>
                    <Appbar.BackAction color={Colors.blue500} onPress={()=>(
                        this.props.navigation.dispatch(NavigationActions.back())
                    )} />
                    <Appbar.Content
                        title={'Post details'}
                    />
                </Appbar.Header>
                <Divider />
                {/* <Text>
                    {JSON.stringify(this.props.forumSelected)}
                </Text> */}
                {/* <TabInfo /> */}

                <FlatList
                    ListHeaderComponent={<ForumComponent primary={true} forum={this.props.forumSelected} />}
                    onRefresh={()=>this.getComments()}
                    refreshing={this.state.loading}
                    // onEndReached={this.onEndReached}
                    
                    // onEndReachedThreshold={4}
                    initialNumToRender={10}
                    maxToRenderPerBatch={10}
                    windowSize={10}

                    data={this.state.postReplies}
                    // style={homeStyles.flatList}
                    renderItem={this.renderItem}
                    // contentContainerStyle={{padding: 10}}
                    keyExtractor={keyExtractor}
                />

                <Divider />
                <View style={{position: 'relative'}}>
                    <TextInput
                        style={{paddingHorizontal: 10, paddingRight: 90}}
                        placeholder='Respond...'
                        multiline={true}
                        value={this.state.post.title}
                        onChangeText={text => this.setState({post:{title: text}})}
                    />
                    <IconButton
                        icon="add-a-photo"
                        color={Colors.grey500}
                        size={20}
                        style={{position: 'absolute', bottom: 0, right: 55}}
                        onPress={() => console.log('Pressed')}
                    />
                    <IconButton
                        icon="send"
                        color={Colors.blue500}
                        size={20}
                        style={{position: 'absolute', bottom: 0, right: 10}}
                        onPress={() => this.create()}
                    />
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addUser: (user)=>dispatch(addUser(user)),
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        forumSelected: state.forumSelected,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PostDetailScreen);
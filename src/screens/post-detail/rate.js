import React, { Component } from 'react';
import { FlatList, StyleSheet } from 'react-native';
import MGRate from '../../components/rate';

export default class TabRate extends Component {
    constructor(props){
        super(props);

        this.state = {
            data: [{
                title: "Группчане, кто может скинуть таблицу соответствия российских размеров колец турецким.Заранее благодарю.",
            },{
                title: "Host fellow travelers to fund your own fun in the sun. Peak hosting season starts now.",
                imgURL: "https://cdn.dribbble.com/users/2046015/screenshots/4392224/dubai-01__0-00-00-00_.png",
            },{
                title: "Americaga Visa",
                imgURL: "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-9/56956523_2136511229766789_5071768047660826624_o.jpg?_nc_cat=111&_nc_ht=scontent-arn2-1.xx&oh=7f88002f76d6e290e1124d703a3ec6cc&oe=5D719AAD"
            }]
        }
    }
    onEndReached = ()=>{
        this.setState({
            data: [...this.state.data, ...[{
                title: "Группчане, кто может скинуть таблицу соответствия российских размеров колец турецким.Заранее благодарю."
            }]]
        });
    }

    _keyExtractor = (item, index) => index+'';

    renderItem = ({item, index})=>(
        <MGRate
            navigation={this.props.navigation}
            title={item.title}
            imgURL={item.imgURL}
        />
    )

    render() {
        const {tabLabel, navigation} = this.props;
        const {data} = this.state;
        return (
            <FlatList
                onRefresh={()=>console.log('--- ')}
                refreshing={false}
                onEndReached={this.onEndReached}
                
                onEndReachedThreshold={4}
                initialNumToRender={4}
                maxToRenderPerBatch={10}
                windowSize={10}

                tabLabel={tabLabel}
                data={data}
                style={{backgroundColor: '#f5f6f9',}}
                renderItem={this.renderItem}
                keyExtractor={this._keyExtractor}
            />
        )
    }
}

const styles = StyleSheet.create({
})
  
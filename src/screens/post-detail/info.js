import React, {Component} from "react";
import { ScrollView, RefreshControl, View } from "react-native";
import { Text, Title, Caption, Divider, Colors, Card, Subheading } from "react-native-paper";
import { material, robotoWeights, iOSColors } from "react-native-typography";
import UserWrap from "../../components/userWrap";

// Firebase
import firebase from 'react-native-firebase';

// Redux
import { connect } from 'react-redux';

import ChatScreen from "../chat";
import ForumComponent from "../home/component/forum";
export default class TabInfo extends Component{
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            // post: null
        }
    }
    componentDidMount(){
        // firebase.firestore()
        // .collection('posts')
        // .doc('zYztGjZVTTuFd6QJOkuR')
        // .get()
        // .then(post=>{
        //     // alert(JSON.stringify(post.data))
        //     this.setState({post: post.data(), loading: false})
        // })
        // .catch(err=>alert(err));
    }
    render(){
        // const {post} = this.prp
        return (
            <ScrollView 
                refreshControl={
                    <RefreshControl
                    refreshing={this.state.loading}
                    onRefresh={()=>(console.log('Loading...'))}
                    />
                }
                
                contentContainerStyle={{flex:1, backgroundColor: Colors.grey200,}}
            >
                {/* <Card>
                    <Card.Content>
                        <Title>
                            {this.state.post.title}
                        </Title>
                        <Text>
                            {this.state.post.description}
                        </Text>
                    </Card.Content>
                </Card> */}
                {/* <Divider /> */}
                {/* <ChatScreen /> */}

                {
                    this.state.post?
                    <ForumComponent primary={true} forum={this.state.post} />
                    :null
                }
                {
                    this.state.post?(
                        [1,2,3,4].map(e=>(
                            <ForumComponent key={e} forum={this.state.post} />
                        ))
                    )
                    :null
                }
            </ScrollView>
        )
    }
}
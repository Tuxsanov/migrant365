import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Picker,
} from 'react-native';
import { ActivityIndicator, List, Appbar, RadioButton, Surface, Button, Colors, Text, TextInput, Divider, Caption, Snackbar } from 'react-native-paper';
import { NavigationActions } from 'react-navigation';
import { PlacesCategoryData } from '../../data/PlacesCategoryData';

// Localize
import * as RNLocalize from "react-native-localize";
import I18n from "../../uitils/I18n";

// Firebase
import firebase from 'react-native-firebase';
import { useAsyncStorage } from '@react-native-community/async-storage';

// Lodash
import { debounce } from 'lodash';

const places = algoliasearch.initPlaces('plOSVXGXKTHA', '0d92fcc32d99d389a1a8ff4370f7ca1a');

import algoliasearch from "algoliasearch";
const client = algoliasearch('2Y3UE5G3UT', 'ac9dfbf6901ebf8cb773ff0378b80ec4');
const index = client.initIndex('city');

export default class RecommendPlacesPage extends Component {
    constructor(props){
        super(props);
        const {countryCode, languageCode} = RNLocalize.getLocales()[0];
        this.state  = {
            countryCode: countryCode.toUpperCase(), 
            languageCode: languageCode.toLowerCase(),
            placesCategory: PlacesCategoryData,
            selectedCategory: null,
            searchResults: [],
            selectedCity: null,
            searching: false,
            searchCityText: '',
            descriptionText: '',
            snakbarVisible: false,
        };
    }

    componentDidMount(){}

    _search = debounce(()=>{
        if(this.state.searchCityText){
            this.setState({searching: true});        
            places.search({
                type: "address",
                query: this.state.searchCityText,
                hitsPerPage: "2",
                countries: [this.state.countryCode.toLowerCase()]
            }, (err, res) => {
                if (err) {
                    throw err;
                }
                this.setState({searchResults: res.hits, searching: false});
            });
        }else{
            this.setState({searchResults: []});
        }
    }, 300);

    searchCity = (query)=>{
        this.setState({searchCityText: query});
        this._search();
    }

    selectCity(country){
        this.setState({selectedCity: country, searchCityText:country.locale_names.default[0], searchResults: []})
    }

    publish(){
        const {selectedCategory, descriptionText, selectedCity, countryCode} = this.state;
        firebase.firestore()
        .collection('city')
        .doc(countryCode)
        .collection('recommend')
        .doc(selectedCategory)
        .collection('data')
        .add({
            category: selectedCategory, 
            description: descriptionText, 
            city: selectedCity,
        }).then(()=>{
            this.setState({
                selectedCategory: null,
                selectedCity: null,
                descriptionText: null,
                searchCityText: null,
                snakbarVisible: true,
            });
        }).catch(e=>{
            alert(e)
        })
    }


    render() {
    const {
        descriptionText,
        searchResults, 
        searching, 
        countryCode, 
        searchCityText,
    } = this.state;
    return (
        <View style={{flex: 1}}>
            <Appbar.Header style={{backgroundColor: '#fff',}}>
                <Appbar.BackAction onPress={()=>(
                    this.props.navigation.dispatch(NavigationActions.back())
                )} />
                <Appbar.Content
                    title={I18n.t(`recomendPlaceScreen.title`, {locale: this.state.languageCode})}
                />
            </Appbar.Header>
            <Divider />
            <ScrollView style={styles.container}>
                <Picker
                    selectedValue={this.state.selectedCategory}
                    style={{height: 50, borderColor: Colors.grey200, borderWidth: 1, width: '100%'}}
                    onValueChange={(itemValue, itemIndex) =>
                        this.setState({selectedCategory: itemValue})
                    }
                >
                    <Picker.Item 
                        label={I18n.t(`actions.pleaseSelectCategory`, {locale: this.state.languageCode})} 
                    />
                    {this.state.placesCategory.map(places=>(
                        <Picker.Item 
                            label={I18n.t(`homeTabPlaces.list.${places.id}`, {locale: this.state.languageCode})} 
                            value={places.id} 
                            key={places.id} 
                        />
                    ))}
                </Picker>
                <TextInput
                    label='Location'
                    style={{backgroundColor: '#fff',}}
                    onChangeText={this.searchCity}
                    value={searchCityText}
                />

                {
                    searching?<ActivityIndicator animating={true} color={Colors.red800} />:
                    <List.Section>
                        {searchResults.map(country=>(
                            <List.Item
                                key={country.objectID}
                                style={{backgroundColor: Colors.grey200,}}
                                title={country.locale_names.default[0]}
                                onPress={()=>this.selectCity(country)}
                            />
                            // <Text key={country.objectID}>
                            //     {JSON.stringify(country.locale_names.default)}
                            // </Text>
                        ))}
                    </List.Section>
                }


                <TextInput
                    label='Description'
                    numberOfLines={4}
                    multiline={true}
                    style={{backgroundColor: '#fff',}}
                    value={descriptionText}
                    onChangeText={text => this.setState({ descriptionText: text })}
                />
                <Button mode="contained" onPress={()=>this.publish()}>
                    {I18n.t(`actions.submit`, {locale: this.state.languageCode})}
                </Button>
                <Caption style={{width: 200}}>
                    {I18n.t(`recomendPlaceScreen.terms`, {locale: this.state.languageCode})}                    
                </Caption>
            </ScrollView>

            <Snackbar
                visible={this.state.snakbarVisible}
                onDismiss={() => this.setState({ snakbarVisible: false })}
            >
                Thanks! Your recomendations sent to review team
            </Snackbar>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
});
import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { material, robotoWeights } from 'react-native-typography';

import { 
    List, Avatar, TextInput,
    Modal, Portal, Text, Button, Provider } from 'react-native-paper';

import { StackActions } from 'react-navigation';
import MGToolbar from '../components/toolbar';

import AsyncStorage from '@react-native-community/async-storage';

const pushAction = StackActions.push({
  routeName: 'PostAdd',
//   params: {
//     myUserId: 9,
//   },
});


export default class SettingsScreen extends Component {
    state = {
        text: '',
        visible: false,
        settings: [{
            title: 'Edit Details',
            icon: 'edit'
        },{
            title: 'Change Password',
            icon: 'edit'
        },{
            title: 'Change Country',
            icon: 'map'
        },{
            title: 'About',
            icon: 'map'
        }]
    };

    _retrieveData = async () => {
        try {
          const value = await AsyncStorage.getItem('uuid');
        //   alert(value);
          if (value !== null) {
            // We have data!!
          }else{

          }
        } catch (error) {
          // Error retrieving data
        }
    };

    _showModal = () => this.setState({ visible: true });
    _hideModal = () => this.setState({ visible: false });

    _modal = ()=>{
        const { visible } = this.state;
        return <Provider>
        <Portal>
          <Modal visible={visible} onDismiss={this._hideModal}>
            <Text>Example Modal</Text>
          </Modal>
          <Button
            style={{ marginTop: 30 }}
            onPress={this._showModal}
          >
            Show
          </Button>
        </Portal>
      </Provider>
    }

    async componentDidMount(){
        this._retrieveData();
    }

    changeRoute=()=>{
        this.props.navigation.dispatch(pushAction);
    }

    render() {
        return (
            <ScrollView style={{flex:1}}>
                <MGToolbar />
                
                <View style={{alignItems: 'center', paddingVertical: 20}}>
                    <Avatar.Text size={90} label="XD" />
                    <Text style={{...material.headline, ...robotoWeights.bold, marginTop: 10,}}>Yuiore Kioes</Text>
                    <Text style={{...material.subheading, ...robotoWeights.light}}>Uzbekistan</Text>
                </View>

                <List.Section>
                    {
                        this.state.settings.map((_setting, index)=>(
                            <List.Item
                                key={index}
                                style={{borderBottomWidth: 1, borderStyle: 'solid', borderColor: 'rgba(0,0,0,0.2)'}}
                                title={_setting.title}
                                left={() => <List.Icon icon={_setting.icon} />}
                            />
                        ))
                    }
                    <List.Item
                        title="Log out"
                        left={() => <List.Icon color="#fff" icon="map" />}
                    />
                </List.Section>
            </ScrollView>
        )
    }
}

import React, { Component } from 'react';
import { Text, Platform, PermissionsAndroid, TouchableOpacity, ScrollView, View, TextInput, Picker, StyleSheet, RefreshControl, FlatList, Image, Dimensions, CameraRoll } from 'react-native';
import { material } from 'react-native-typography';
import { Chip, Colors, Divider, Avatar, IconButton, TouchableRipple, Surface } from 'react-native-paper';

// Firebase
import firebase from 'react-native-firebase';
const fs = firebase.firestore();
// Redux
import { connect } from 'react-redux';

// Material Icons
import { NavigationActions, StackActions } from 'react-navigation';
import { updateForm } from '../../../redux/actions/form';

const TOOLBAR_HEIGHT  = 56;
const ICON_SIZE = 24;

const TOOLBAR_ELEVATION = 4;

const CONTAINER_PADDING = 16;

const COLOR_PRIMARY = Colors.red500;

// 
// import { readFile } from "react-native-fs";
import * as RNFS from 'react-native-fs';

import Permissions from 'react-native-permissions';

class AddPostFormScreen extends Component {
    constructor(props){
        super(props);
        
        this.state = {
            loading: false,
        };
    }

    create = async () => {
        try {
            this.setState({loading: true});
            // const captureImages = JSON.stringify(event.captureImages);
            // let img = event.captureImages[0].uri;
            let uri = await RNFS.readFile(`${this.props.form.selectedFormImgs[0]}`, 'base64');
            await firebase.storage()
            .ref(`postImg/${this.props.user.uid +'_'+ Math.ceil(Math.random()*1000)}`)
            // .
            .putFile(this.props.form.selectedFormImgs[0]);
    
            this.setState({loading: false});
        }catch(err){
            alert(err);
        }
        // alert(uri.length)
        // await readFile(this.props.form.selectedFormImgs[0], "base64")
        // .then(contents => {
        //     alert('contents.length');
        //   //content is base64 do seomthing wtih it.
        // });
        // alert('Completed')
        // fs.collection('posts').add(post)
        // .then(()=>{
        //     alert('Created!');
        // })
        // .catch(e=>alert(e))
    }

    componentWillUnmount(){}

    componentDidMount() {
        // Permissions.check('storage').then(response => {
        //     // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
        //     // this.setState({ photoPermission: response })
        //     alert(response)
        // })
    }

    log = ()=>alert('log');
    close = ()=>(this.props.navigation.dispatch(NavigationActions.back()));
    
    changeRoute = (routeName)=>{
        const pushAction = StackActions.push({routeName});
        this.props.navigation.dispatch(pushAction);
    }

    deleteSelectImg = (key)=>{
        let imgs = this.props.form.selectedFormImgs;
        const index = imgs.findIndex((e)=>e==key);
        delete imgs[index];

        this.props.updateForm({
            selectedFormImgs: imgs 
        });
    }

    render() {
        return (
            <View style={{flex:1}}>
                <Surface style={{...styles.row_center_hor, height: TOOLBAR_HEIGHT, elevation: TOOLBAR_ELEVATION}}>
                    
                    {/* CLOSE BTN */}
                    <View style={{width: TOOLBAR_HEIGHT}}>{
                        <IconButton
                            icon={'close'}
                            color={Colors.grey600}
                            size={24}
                            onPress={() => this.close()}
                        />
                    }</View>
                    
                    {/* SELECTING CITY */}
                    <TouchableOpacity activeOpacity={1} onPress={()=>this.changeRoute('SelectCity')}>
                        <View style={{flex: 1}}>
                            {/* <Avatar.Image 
                                size={Math.ceil((ICON_SIZE/2.5)*3)}
                                style={{marginRight: 10,}} 
                                source={{uri: `https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${this.props.user.selectedCountry || 'uz'}.png`}} 
                            /> */}
                            <Text style={[material.title]}>{this.props.user.selectedCity?this.props.user.selectedCity:'All cities'}</Text>
                            <Text style={[material.subheading]}>{this.props.user.selectedCountryName}</Text>
                        </View>
                    </TouchableOpacity>

                    {/* PUBLISH */}
                    <IconButton
                        style={{marginLeft: 'auto'}}
                        icon="check"
                        color={COLOR_PRIMARY}
                        size={ICON_SIZE}
                        onPress={() => this.create()}
                    />
                </Surface>

                <ScrollView
                    refreshControl={
                        <RefreshControl refreshing={this.state.loading} />
                    }
                >
                    <View style={{padding: CONTAINER_PADDING}}>
                        <TextInput
                            style={[material.headline, {width: '100%'}]}
                            mode='outlined'
                            multiline={true}
                            placeholder='Write'
                            autoFocus={true}
                            placeholderTextColor={Colors.grey500}
                            // value={this.state.post.content}
                            onChangeText={content => this.setState({post: {...this.state.post, content}})}
                        />
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                        {
                            Array.isArray(this.props.form.selectedFormImgs)&&
                            this.props.form.selectedFormImgs.map((img, index)=>(
                                <TouchableOpacity
                                    style={{
                                        position: 'relative',
                                        width: Dimensions.get('window').width/this.props.form.selectedFormImgs.length, 
                                        height: 300/this.props.form.selectedFormImgs.length
                                    }}
                                    activeOpacity={1}
                                    key={index}
                                >
                                    <IconButton
                                        icon={'close'}
                                        color={Colors.red600}
                                        size={20}
                                        style={{position: 'absolute', zIndex: 2, right: 0, top: 0}}
                                        onPress={() => this.deleteSelectImg(img)}
                                    />
                                    <Image
                                        resizeMode={"cover"}
                                        style={{
                                            width: Dimensions.get('window').width/this.props.form.selectedFormImgs.length, 
                                            height: 300/this.props.form.selectedFormImgs.length,
                                            borderWidth: 2,
                                            borderColor: Colors.white,
                                            borderRadius: 6,
                                        }}
                                        source={{uri: `file://${img}`}}
                                    />
                                </TouchableOpacity>
                            ))
                        }
                    </View>
                </ScrollView>
                <Divider />
                <View style={{flexDirection: 'row', padding: 16,}}>
                    <Chip 
                        icon="layers" 
                        style={{marginRight: 8, backgroundColor: Colors.yellow500}} 
                        onPress={()=>this.changeRoute('PostAdd')}
                    >All category</Chip>
                    <Chip 
                        icon="collections" 
                        style={{marginRight: 8}} 
                        onPress={()=>this.changeRoute('ImageAlbums')}                        
                    >Attach images</Chip>
                </View>
            </View>
        )
    }
}

class Touch extends React.PureComponent{
    render(){
        return (
            <TouchableRipple onPress={this.props.onPress}>
                {this.props.children}
            </TouchableRipple>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addUser: (user)=>dispatch(addUser(user)),
        updateForm: (form)=>dispatch(updateForm(form)),
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        form: state.form,
    }
};

const styles = StyleSheet.create({
    row_center_hor: {
        flexDirection: 'row',
        alignItems: 'center',
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(AddPostFormScreen);
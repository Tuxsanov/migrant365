import React, { Component } from 'react';
import { StatusBar, Image, Text, View, ScrollView, } from 'react-native';
import { List, Avatar, Title, HelperText, Button, TextInput, Divider, Colors, Appbar, Subheading, Caption, } from 'react-native-paper';

import { StackActions } from 'react-navigation';
import MGToolbar from '../../components/toolbar';

import { AccessToken, LoginManager } from 'react-native-fbsdk';

const registerPushAction = StackActions.push({
  routeName: 'Register',
});

// Firebase
import firebase from 'react-native-firebase';
import AsyncStorage, { useAsyncStorage } from '@react-native-community/async-storage';
import { facebookLogin } from '../../auth/facebook';
import { googleLogin } from '../../auth/google';
import { emailLogin } from '../../auth/login';
import { getData } from '../../storage';
import MGToolbarBack from '../../components/toolbar-back';
import { translate } from '../../functions/translate';

export default class LoginScreen extends Component {

    state = {
        prestine: true,
        errEmailOrPassIncorrect: false,
        email: '',
        password: '',
    };

    register=()=>{
        this.props.navigation.dispatch(registerPushAction);
    }

    validateEmail(email) {
        if(!email&&this.state.prestine){
            return true;
        }else{
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
    }

    validatePassword(password){
        if(!password&&this.state.prestine){return true}
        return password.length>=8
    }

    loginWithFb(){
        LoginManager.logInWithReadPermissions(['public_profile', 'email'])
        .then(result=>{
            if (result.isCancelled) {
                return;
            }
            AccessToken.getCurrentAccessToken().then((fbToken)=>{
                const credential = firebase.auth.FacebookAuthProvider.credential(fbToken.accessToken);
                alert(user.user.uid)
                firebase.auth().signInWithCredential(credential)
                .then(user=>{
                    alert(user);
                    firebase.firestore().collection('user').doc(user.user.uid)
                    .get().then(snapshot=>{
                        if(!snapshot.exists){
                            snapshot.ref.set({
                                email: user.user.email,
                                avatarImg: user.user.photoURL,
                                fullName: user.user.displayName,
                            }).then(()=>{
                                this.props.navigation.dispatch(
                                    StackActions.push({
                                        routeName: 'Profile',
                                    })
                                )
                            });
                        }else{
                            this.props.navigation.dispatch(
                                StackActions.push({
                                    routeName: 'Profile',
                                })
                            )
                        }
                      })
                })
                .catch(err=>{
                    alert(err);
                });
                // AsyncStorage.setItem('uid', user.user.uid).then(()=>{
                // }).catch(err=>alert(err));
                

            }).catch(err=>{});
        })
        .catch(err=>{});
    }

    loginWithGoogle(){
        return googleLogin();
    }

    componentDidMount(){
        // firebase.auth().signOut().then()
        // this.loginWithEmail();
    }
    
    async loginWithEmail(){
        // let email = 'mr.jhon1327@gmail.com';
        // let password = '12345678';
        // try {
        //     const loginUserCredentials = await firebase.auth()
        //     .signInWithEmailAndPassword(email, password)
          
        //     alert(loginUserCredentials.user.uid)
        //     // if(loginUserCredentials.user){
        //     //   storeData('user', JSON.stringify(loginUserCredentials.user));
        //     // }
        
        // } catch (e) {
        // alert(e);
        // }
        // try {
        //     let email = await emailLogin('mr.jhon1327@gmail.com', '12345678');
        //     alert(email);
        // }catch(err){
        //     alert(errr)
        // }
        // alert('++')
        firebase.auth().signInAnonymously().then(()=>{
            // AsyncStorage.setItem('uid', 'hqnMEgHhEoempv801S4ueTTjAR53').then(()=>{
            // }).catch(err=>alert(err));
            alert('Logged in!')
        }, err=>{alert(err.message)});
    }

    render() {
        return (
            <View style={{flex:1}}>
                <MGToolbarBack title={translate('loginScreen.title')} navigation={this.props.navigation} />
                
                <ScrollView style={{backgroundColor: Colors.white,}}>
                    <View style={{paddingHorizontal: 16, paddingVertical: 16,}}>
                        <TextInput
                            error={!this.validateEmail(this.state.email)}
                            style={{backgroundColor: Colors.white}}
                            label={translate('form.phoneNumber')}
                            mode={'outlined'}
                            keyboardType={'phone-pad'}
                            value={this.state.email}
                            onChangeText={email => this.setState({ email })}
                        />
                        {/* <HelperText
                            type="error"
                            visible={!this.validateEmail(this.state.email)}
                        >
                            {translate('formError.emailIncorrect')}
                        </HelperText>
                        <TextInput
                            error={!this.validatePassword(this.state.password)}
                            style={{backgroundColor: Colors.white}}
                            label={translate('form.password')}
                            keyboardType={'numbers-and-punctuation'}
                            secureTextEntry={true}
                            value={this.state.password}
                            onChangeText={password => this.setState({ password })}
                        />
                        <HelperText
                            type="error"
                            visible={!this.validatePassword(this.state.password)}
                        >
                            {translate('formError.passwordLengthShort')}
                        </HelperText> */}
                        {/* {
                            this.state.errEmailOrPassIncorrect?
                            <HelperText
                                type="error"
                            >
                                {translate('formError.emailOrPassIncorrect')}
                            </HelperText>:
                            null
                        }
                        <Button uppercase={false} style={{marginTop: 16}} mode="text" onPress={() => console.log('Pressed')}>
                            {translate('actions.forgotPassword')}
                        </Button> */}
                        <Button uppercase={false} style={{marginTop: 16, backgroundColor: Colors.red500}} mode="contained" onPress={() => this.loginWithEmail()}>
                            {translate('actions.sen')}
                        </Button>
                    </View>
                    {/* <Divider />
                    <View style={{paddingHorizontal: 16, paddingVertical: 16,}}>
                        <Button uppercase={false} style={{borderColor: Colors.blue500}} color={Colors.blue500} mode="outlined" onPress={() => this.loginWithFb()}>
                            {translate('actions.signInWithFaceBook')}
                        </Button>
                        <Button uppercase={false} style={{marginTop: 16, borderColor: Colors.red500}} color={Colors.red500} mode="outlined" onPress={() => this.loginWithGoogle()}>
                            {translate('actions.signInWithGoogle')}
                        </Button>
                    </View>
                    <Divider />
                    <View style={{paddingHorizontal: 16, paddingVertical: 16,}}>
                        <Button uppercase={false} style={{borderColor: Colors.grey800}} color={Colors.grey800} mode="outlined" onPress={() => this.register()}>
                            {translate('actions.createAnAccount')}
                        </Button>
                    </View> */}
                </ScrollView>
            </View>
        )
    }
}

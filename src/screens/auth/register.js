import React, { Component } from 'react';
import { ProgressBarAndroid, StatusBar, Image, Text, View, ScrollView, } from 'react-native';
import { List, Avatar, Title, HelperText, Button, TextInput, Divider, Colors, Appbar, Subheading, Caption, } from 'react-native-paper';

import { StackActions } from 'react-navigation';
import MGToolbar from '../../components/toolbar';

const pushAction = StackActions.push({
  routeName: 'Home',
});

// Firebase
import firebase from 'react-native-firebase';
import AsyncStorage, { useAsyncStorage } from '@react-native-community/async-storage';
import MGToolbarBack from '../../components/toolbar-back';
import { translate } from '../../functions/translate';

export default class RegisterScreen extends Component {

    constructor(props){
        super(props);

        this.state = {
            loading: false,
            prestine: true,
            errEmailInUse: false,
            email: '',
            password: '',
            fullName: '',
        };
    }

    changeRoute=()=>{
        this.props.navigation.dispatch(pushAction);
    }

    validateEmail(email) {
        if(!email&&this.state.prestine){
            return true;
        }else{
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
    }

    validatePassword(password){
        if(!password&&this.state.prestine){return true}
        return password.length>=8
    }

    componentDidMount(){
    }

    async registerWithEmail(){
        this.setState({errEmailInUse: false, prestine: false});
        if(!this.state.email || !this.state.password){return}
        try {
            this.setState({loading: true});
            const registerUserCredentials = await firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
        
            if(registerUserCredentials.user){
                AsyncStorage.setItem('uid', registerUserCredentials.user.uid);
            }

            firebase.firestore()
            .collection('user')
            .doc(registerUserCredentials.user.uid)
            .set({
                email: this.state.email,
                fullName: this.state.fullName,
            })
            .then(e=>{
                this.setState({loading: false});
                this.changeRoute();
            })
            .catch(e=>{
                alert(e.code);
                this.setState({loading: false});
            });
        } catch (e) {
            if(e.code == 'auth/email-already-in-use'){
                this.setState({errEmailInUse: true})
            }
            this.setState({loading: false});
        }
    }

    render() {
        const {loading} = this.state;
        return (
            <View style={{flex:1}}>
                <MGToolbarBack title={translate('registerScreen.title')} navigation={this.props.navigation} />
                
                <ScrollView style={{backgroundColor: Colors.white,}}>
                    <View style={{paddingHorizontal: 16, paddingVertical: 16,}}>
                        <TextInput
                            disabled={loading}
                            style={{backgroundColor: Colors.white}}
                            label={translate('form.fullName')}
                            mode={'flat'}
                            value={this.state.fullName}
                            onChangeText={fullName => this.setState({ fullName })}
                        />
                        <TextInput
                            disabled={loading}
                            error={!this.validateEmail(this.state.email) || this.state.errEmailInUse}
                            style={{backgroundColor: Colors.white}}
                            label={translate('form.email')}
                            mode={'flat'}
                            keyboardType={'email-address'}
                            value={this.state.email}
                            onChangeText={email => this.setState({ email })}
                        />
                        {
                            this.state.errEmailInUse?
                            <HelperText
                                type="error"
                            >
                                {translate('formError.emailAlreadyinUse')}
                            </HelperText>:null
                        }
                        {
                            !this.validateEmail(this.state.email)?
                            <HelperText
                                type="error"
                            >
                                {translate('formError.emailIncorrect')}
                            </HelperText>:null
                        }
                        <TextInput
                            disabled={loading}
                            error={!this.validatePassword(this.state.password)}
                            style={{backgroundColor: Colors.white}}
                            label={translate('form.password')}
                            keyboardType={'numbers-and-punctuation'}
                            secureTextEntry={true}
                            value={this.state.password}
                            onChangeText={password => this.setState({ password })}
                        />
                        <HelperText
                            type="error"
                            visible={!this.validatePassword(this.state.password)}
                        >
                            {translate('formError.passwordLengthShort')}
                        </HelperText>
                        <Button disabled={loading} uppercase={false} style={{backgroundColor: Colors.red500}} mode="contained" onPress={() => this.registerWithEmail()}>
                            {translate('actions.createAnAccount')}
                        </Button>
                        <Caption>
                            By clicking Create and Account button you agree with the terms and conditions
                        </Caption>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

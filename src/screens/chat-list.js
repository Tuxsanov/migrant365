import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { material, robotoWeights } from 'react-native-typography';
import { TouchableRipple, List, Avatar, Button, TextInput } from 'react-native-paper';

import { StackActions } from 'react-navigation';
import MGToolbar from '../components/toolbar';

const pushAction = StackActions.push({
  routeName: 'ChatScreen',
});


export default class ChatListScreen extends Component {
    state = {
        text: '',
        users: [{
            name: 'Kathleen Kelly',
            url: 'https://uinames.com/api/photos/female/25.jpg'
        },{
            name: 'Bobby Young',
            url: 'https://uinames.com/api/photos/male/5.jpg'
        },{
            name: 'Bobby Young',
            url: 'https://uinames.com/api/photos/male/2.jpg'
        },{
            name: 'Eugene Stewart',
            url: 'https://uinames.com/api/photos/female/8.jpg'
        },{
            name: 'Terry Reynolds',
            url: 'https://uinames.com/api/photos/male/11.jpg'
        }]
    };

    changeRoute=()=>{
        this.props.navigation.dispatch(pushAction);
    }

    render() {
        return (
            <ScrollView style={{flex:1}}>
                {/* <MGToolbar /> */}
                {
                    this.state.users.map((_setting, index)=>(
                        <TouchableRipple
                            onPress={()=>this.changeRoute()}
                            key={index}
                            style={{borderTopWidth: 1, borderStyle: 'solid', borderColor: 'rgba(0,0,0,0.2)'}}
                        >
                            <View style={{paddingHorizontal: 15, paddingVertical: 10, flexDirection: 'row'}}>
                                <Avatar.Image size={40} source={{uri: _setting.url}} />
                                <View style={{flex:1, flexWrap: 'wrap', flexDirection: 'column', marginLeft: 10}}>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={material.button} >{_setting.name}</Text>
                                        <Text style={{...material.caption, ...robotoWeights.bold, marginLeft: 'auto'}} >15:34 12/03/2019</Text>
                                    </View>
                                    <Text style={material.caption} >I have made this aggregator to speed up the process while designing UI and in need to populate user avatars with real looking photos. </Text>
                                </View>
                            </View>
                        </TouchableRipple>
                    ))
                }
            </ScrollView>
        )
    }
}

import React, {Component} from 'react';
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
} from 'react-native';
import { iOSColors, material, robotoWeights } from 'react-native-typography';
import { Button, Paragraph, Menu, Divider, Provider, List, Switch, Checkbox } from 'react-native-paper';

export default class OrderHomePosts extends Component {
  
  state = {
    isSwitchOn: false,
    checked: true,
  };


  render() {
    const { isSwitchOn, checked } = this.state;
    return (
      <ScrollView style={styles.container}>
        <List.Section>
          <List.Accordion
            title="Order by date"
          >
            <List.Item title="By time" titleStyle={[robotoWeights.bold, {color: iOSColors.red}]} right={() => <List.Icon color={iOSColors.red} icon="check-circle" />} />
            <List.Item title="By week" />
          </List.Accordion>
          <Divider />
          <List.Accordion
            title="Order by date"
          >
            <List.Item
              title="All"
              right={() => <Checkbox
                status={isSwitchOn ? 'checked' : 'unchecked'}
                onPress={() => { this.setState({ isSwitchOn: !isSwitchOn }); }}
              />}
            />
            <List.Item
              title="Travel"
              titleStyle={[robotoWeights.bold, {color: iOSColors.red}]}
              right={() => <Checkbox
                status={checked ? 'checked' : 'unchecked'}
                onPress={() => { this.setState({ checked: !checked }); }}
              />}
            />
          </List.Accordion>
      </List.Section>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // padding: 20,
  },
});
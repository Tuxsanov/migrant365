import React, {Component} from 'react';
import { View } from 'react-native';
import { Avatar, Card, IconButton, Text, Title, Caption, Colors, Headline, Subheading } from 'react-native-paper';
import { material, robotoWeights } from 'react-native-typography';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MGAvatar from '../../../components/avatar';
export default class CardTitleComponent extends Component {
    render(){
        const {user} = this.props;
        const {name, lastname,} = user;
        return (
            <View>

                <View style={{
                    flexDirection: 'row',
                    // alignItems: 'center',
                    justifyContent: 'space-between',
                    // paddingLeft: 16,
                    // paddingTop: 10,
                    // height: 50,
                }}>
                    {/* <MGAvatar user={user} size={40}/> */}
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        // marginLeft: 10,
                    }}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={{...material.subheading, ...robotoWeights.medium}}>
                                {name} {lastname}
                            </Text>
                            <MaterialIcons color={Colors.blue500} name="verified-user" size={16} style={{marginLeft: 2}} />                            
                            <View style={{width: 2, height: 2, borderRadius: 2,backgroundColor: Colors.grey900, marginHorizontal: 4}}></View>
                            <Text style={{...material.subheading, ...robotoWeights.light}}>
                                12h
                            </Text>

                            {/* <MaterialIcons color={Colors.blue500} name="videocam" size={14} style={{marginLeft: 6}} /> */}
                        </View>
                        {/* <Caption style={{
                            minHeight: 20,
                            marginVertical: 0,
                        }} numberOfLines={1}>
                            16:00 17.06.2019
                        </Caption> */}
                    </View>
                    {/* <View>
                        <IconButton
                            icon="more-horiz"
                            size={24}
                            style={{marginLeft: 'auto'}}
                            onPress={() => console.log('Pressed')}
                        />
                    </View> */}
                </View>
            </View>
        )
    }
}
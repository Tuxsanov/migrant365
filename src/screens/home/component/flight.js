import React from 'react';
import { View } from 'react-native';
import { CardTitleComponent } from "./card-title";
import { Card, List, Subheading, Headline, Text, Title, Paragraph, Divider, Button, Caption, Chip } from "react-native-paper";
import { material, robotoWeights } from 'react-native-typography';

export const FlightComponent = ({data}) => (
    <Card style={[{marginBottom: 10, }]}>
        <CardTitleComponent />
        <Card.Content>
            <View style={{marginBottom: 5,flexDirection: 'row'}}>
                <View style={{width: 60, alignItems: 'center'}}>
                </View>
                <View>
                    <Chip icon="flight-takeoff" onPress={() => console.log('Pressed')}>By Plane</Chip>
                </View>
            </View>
            <View style={{marginBottom: 5,flexDirection: 'row'}}>
                <View style={{width: 60, alignItems: 'center'}}>
                    <Subheading>
                        17:00
                    </Subheading>
                </View>
                <View style={{width: 200}}>
                    <Subheading style={[robotoWeights.bold, {marginBottom: 0}]}>
                        Москва
                    </Subheading>
                    <Caption>17.06.2109</Caption>
                </View>
            </View>
            <View style={{marginBottom: 5,flexDirection: 'row'}}>
                <View style={{width: 60, alignItems: 'center'}}>
                    <Subheading>
                        18:15
                    </Subheading>
                </View>
                <View style={{width: 200}}>
                    <Subheading style={[robotoWeights.bold, {marginBottom: 0}]}>
                        Санкт-Петербург
                    </Subheading>
                    <Caption>17.06.2109</Caption>
                </View>
            </View>
        </Card.Content>
    </Card>
);
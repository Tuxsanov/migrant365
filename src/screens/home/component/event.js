import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Button, Avatar, Text, IconButton, Divider, Subheading, Card, Colors, Paragraph, Title, Caption } from 'react-native-paper';
import { material, robotoWeights } from 'react-native-typography';
import { changeRoute } from '../../../functions/route';
import { CardTitleComponent } from './card-title';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class EventComponent extends Component {
    renderSeparator(){
        return(
            <View style={styles.separator}></View>
        )
    }
    render() {
        const {item} = this.props;
        return (
            <View style={{backgroundColor: Colors.white, marginTop: 5,}}>
                <View style={[{marginTop: 5, padding: 10, paddingHorizontal: 15}]}>
                    <TouchableOpacity activeOpacity={1} onPress={() => changeRoute(this.props.navigation, 'EventDetails')}>
                        <View style={{flexDirection: 'row', paddingTop: 6}}>
                            <View style={{flex: 1, flexWrap: 'wrap',}}>
                                <View style={{flexDirection: 'row', alignItems: 'center',}}>
                                    <Avatar.Image style={{marginRight: 4}} size={50} source={{uri: 'https://uinames.com/api/photos/female/9.jpg'}} />
                                    <View style={{marginLeft: 6}}>
                                        <View style={{flexDirection: 'row', alignItems: 'center',}}>
                                            <Text style={{...material.subheading, margin: 0, ...robotoWeights.medium}}>
                                                Janes Doe
                                            </Text>
                                            <MaterialIcons color={Colors.blue500} name="verified-user" size={16} style={{marginLeft: 2}} />                            
                                            {this.renderSeparator()}
                                            <Text style={{...material.subheading, margin: 0, ...robotoWeights.light}}>
                                                12h
                                            </Text>
                                        </View>
                                        <View style={{flexDirection: 'row', alignItems: 'center',}}>
                                            <Text style={{...material.caption, fontSize: 12,}}>
                                                TRAVEL
                                            </Text>
                                            {this.renderSeparator()}
                                        </View>
                                    </View>
                                </View>
                                <View style={{marginBottom: 5}}>
                                    <Title numberOfLines={1} style={{marginBottom: 0}}>{item.title}</Title>
                                    <Paragraph numberOfLines={1} style={material.body1}>{item.subtitle}</Paragraph>
                                </View>
                                <Divider />                            
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5}}>
                                    <Subheading>
                                        Going
                                    </Subheading>
                                    <View style={{flexDirection: 'row'}}>
                                        <Avatar.Image style={{marginRight: 4}} size={30} source={{uri: 'https://uinames.com/api/photos/male/19.jpg'}} />
                                        <Avatar.Image style={{marginRight: 4}} size={30} source={{uri: 'https://uinames.com/api/photos/female/9.jpg'}} />
                                        <Avatar.Image style={{marginRight: 4}} size={30} source={{uri: 'https://uinames.com/api/photos/male/11.jpg'}} />
                                        <Avatar.Image style={{marginRight: 4}} size={30} source={{uri: 'https://uinames.com/api/photos/male/9.jpg'}} />
                                        <Avatar.Text label="+12" style={{marginRight: 4,}} size={30} />
                                    </View>
                                </View>
                                <Divider />
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5}}>
                                    <Subheading>
                                        Maybe
                                    </Subheading>
                                    <View style={{flexDirection: 'row'}}>
                                        <Avatar.Image style={{marginRight: 4}} size={30} source={{uri: 'https://uinames.com/api/photos/female/1.jpg'}} />
                                        <Avatar.Image style={{marginRight: 4}} size={30} source={{uri: 'https://uinames.com/api/photos/female/11.jpg'}} />
                                        {/* <Avatar.Image style={{marginRight: 4}} size={30} source={{uri: 'https://uinames.com/api/photos/male/11.jpg'}} /> */}
                                        <Avatar.Image style={{marginRight: 4}} size={30} source={{uri: 'https://uinames.com/api/photos/male/9.jpg'}} />
                                        <Avatar.Text label="+12" style={{marginRight: 4,}} size={30} />
                                    </View>
                                </View>
                                <Divider />
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5}}>
                                    <Subheading>
                                        Time
                                    </Subheading>
                                    <Subheading>
                                        17:00 14 June 2018
                                    </Subheading>
                                </View>
                                <Divider />
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5}}>
                                    <Subheading>
                                        Location
                                    </Subheading>
                                    <Subheading>
                                        Moscow, Russia
                                    </Subheading>
                                </View>
                                <Divider />
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5}}>
                                    <Subheading>
                                        Price
                                    </Subheading>
                                    <Subheading>
                                        Free
                                    </Subheading>
                                </View>
                                {/* <Divider /> */}
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View style={{width: '50%'}}>
                            <Button icon="done" mode={'text'} onPress={() => console.log('Pressed')}>
                                Attend
                            </Button>
                        </View>
                        <View style={{width: '50%'}}>
                            <Button icon="favorite-border" mode={'text'} onPress={() => console.log('Pressed')}>
                                Maybe
                            </Button>
                        </View>
                    </View>
                </View>
                <Divider />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    separator: {
        width: 2, 
        height: 2, 
        borderRadius: 1,
        backgroundColor: Colors.grey300, 
        marginHorizontal: 4
    }
})
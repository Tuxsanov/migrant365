import React from 'react';
import {View} from 'react-native';
import {Colors, List, Banner, Surface} from 'react-native-paper';

import { changeRoute } from '../../../functions/route';
import { translate } from '../../../functions/translate';
import { robotoWeights } from 'react-native-typography';

export default class ListHeaderComponent extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            bannerVisible: true,
            filter: [{
                id: 'selectCity',
                icon: 'map',
                translate: 'actions.selectCity',
                navigate: 'SelectCity',
                selected: null
            },{
                id: 'selectCategory',
                icon: 'label',
                translate: 'actions.selectCategory',
                navigate: 'SelectCity',
                selected: translate('form.allCategory')
            }]
        }
    }

    componentDidMount(){
    }
    
    componentDidUpdate(prevProps) {
        if (this.props.user.selectedCity !== prevProps.user.selectedCity) {
            let filter = this.state.filter;
            filter[0].selected = this.props.user.selectedCity + '(uzbeks)';
            this.setState({
                filter
            })
        }

        if(!this.props.user.selectedCountry){
            let filter = this.state.filter;
            filter[0].translate = 'actions.selectNationality';
            filter[0].selected = translate('form.allNationality'),
            this.setState({
                filter
            })
        }
    }

    render(){
        return (
            <View>
                <Banner
                    visible={this.state.bannerVisible}
                    actions={[{
                        label: translate('actions.close'),
                        onPress: () => this.setState({ bannerVisible: false }),
                    }]}
                >
                    Узнайте больше о городе, общаясь на форуме. Спросите, ответьте и чувствуйте себя в безопасности
                </Banner>
                {/* <List.Section style={{elevation: 1, backgroundColor: Colors.white}}>
                    <List.Subheader>{translate('form.filter')}</List.Subheader>
                    {
                        this.state.filter.map((filter, index)=>(
                            <List.Item
                                key={filter.id}
                                title={translate(filter.translate)}
                                description={filter.selected}
                                descriptionStyle={[robotoWeights.bold, {color: index==0?Colors.red500:null}]}
                                onPress={()=>changeRoute(this.props.navigation, filter.navigate, 'push')}
                                left={props => <List.Icon {...props} icon={filter.icon} />}
                                right={props => <List.Icon {...props} icon="chevron-right" />}
                            />
                        ))
                    }
                </List.Section> */}
            </View>
        )
    }
}
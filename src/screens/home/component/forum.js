import React, { Component } from 'react';
import { View, StyleSheet, Image, Dimensions, TouchableOpacity, } from 'react-native';
import { Colors, Avatar, IconButton, Divider, Button, Paragraph, Card, Caption, Text, Title, Headline, Subheading, Surface, TouchableRipple, } from 'react-native-paper';
import { iOSColors, material, robotoWeights, } from 'react-native-typography';
import { changeRoute } from '../../../functions/route';
import CardTitleComponent from './card-title';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';


// Firebase
import firebase from 'react-native-firebase';

// Redux
import { connect } from 'react-redux';
import { selectForum } from '../../../../redux/actions/forum';
import MGAvatar from '../../../components/avatar';
import Emoji from 'react-native-emoji';
import { StackActions } from 'react-navigation';


class ForumComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            liked: null,
            screenWidth: 0,
        };
    }

    checkLike = ()=>{
        // firebase.firestore()
        //     .collection('posts')
        //     .doc(this.props.forum.id)
        //     .collection('likes')
        //     .doc(this.props.forum.user.uid)
        //     .get()
        //     .then(snapshot=>{
        //         if(snapshot.data()){
        //             this.setState({liked: snapshot.data().liked})
        //         }
        //     })
        //     .catch(e=>alert(e));
    }

    getUser = ()=>{
        firebase.firestore()
            .collection('user')
            .doc(this.props.forum.uid)
            .get()
            .then(snapshot=>{
                if(snapshot.data()){
                    this.setState({user: snapshot.data()})
                }
            })
            .catch(e=>alert(e));
    }

    componentDidMount(){
        // this.setState({user: this.props.forum.user})
        // this.checkLike();
        this.getUser();

        const screenWidth = Dimensions.get('window').width;
        this.setState({screenWidth});
    }

    like = ()=>{
        console.log('++');
        return;
        if(this.state.liked == 1){
            this.makeDefault();
            return;
        }
        this.setState({liked: 1});
        firebase.firestore()
            .collection('posts')
            .doc(this.props.forum.id)
            .collection('likes')
            .doc(this.props.forum.user.uid)
            .set({liked: 1})
            .then(()=>{
                // this.checkLike();
            })
            .catch(e=>alert(e));
    }
    dislike = ()=>{
        // const fs = firebase.firestore()
        // .collection('posts')
        // .doc(this.props.forum.id)
        // .collection('likes')
        // .doc(this.props.forum.user.uid);

        // if(this.state.liked == 0){
        //     this.makeDefault();
        //     return;
        // }
        
        // this.setState({liked: 0});
        // fs
        // .set({liked: 0})
        // .then(()=>{
        //     // this.checkLike();
        // })
        // .catch(e=>alert(e));
    }

    makeDefault = ()=>{
        // const fs = firebase.firestore()
        // .collection('posts')
        // .doc(this.props.forum.id)
        // .collection('likes')
        // .doc(this.props.forum.user.uid)
        // .set({liked: -1})
        // .then(()=>{
        //     this.setState({liked: null})
        //     // this.checkLike();
        // })
        // .catch(e=>alert(e));
    }

    navigate = ()=>{
        this.props.selectForum(this.props.forum);
        changeRoute(this.props.navigation, 'PostDetails');
    }

    changeRoute = (routeName)=>{
        const pushAction = StackActions.push({
            routeName
        });
        
        this.props.navigation.dispatch(pushAction);
    }

    render() {
        let unread = true;
        // 
        const {liked, user={}} = this.state;
        const {forum = {}, primary} = this.props;
        const {title, description, likes=0, dislikes=0, comments=0} = forum;
        // user={},
        return (
            <Surface style={{marginTop: 8, elevation: 1}}>
                <TouchableOpacity activeOpacity={1} onPress={()=>this.changeRoute('PostDetails')}>
                    <View>
                        {/* <Divider /> */}
                        <View style={styles.wrapper}>
                            <TouchableOpacity activeOpacity={1} onPress={()=>alert('++')}>
                                <View style={styles.leftSide}>
                                    <Avatar.Image size={45} source={{uri: user.avatarImg}} />
                                </View>
                            </TouchableOpacity>
                            <View style={styles.rightSide}>
                                <View style={{flexDirection: 'row', alignItems: 'center',}}>
                                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                                        <MaterialIcons color={Colors.grey500} name="beach-access" size={12} />
                                        <Text style={[material.caption, {marginLeft: 4}]}>Travel</Text>
                                    </View>
                                    <View style={styles.separator}></View>
                                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                                        <MaterialIcons color={Colors.grey500} name="place" size={12} />
                                        <Text style={[material.caption, robotoWeights.bold, {marginLeft: 4}]}>Moscow</Text>
                                        {/* <Emoji name="ru" style={{fontSize: 12}} />                                 */}
                                    </View>
                                    <View style={styles.separator}></View>
                                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                                        <MaterialIcons color={Colors.grey500} name="public" size={12} />
                                        <Text style={[material.caption, {marginLeft: 4}]}>Uzbek, Tajik</Text>
                                    </View>
                                </View>
                                {/* <Divider /> */}
                                <PostHeader date={this.props.forum.date} user={user} />
                                <Text style={[material.body1]} 
                                numberOfLines={5}
                                >{this.props.forum.content}</Text>
                                {
                                    this.props.forum.cover?
                                    <View style={{flexDirection: 'row', marginTop: 8}}>
                                        <Image 
                                            style={{
                                                width: (this.state.screenWidth - 86)/1, 
                                                height: (this.state.screenWidth - 86)/1, borderRadius: 4,
                                            }} 
                                            source={{uri: this.props.forum.cover}} />
                                    </View>
                                    :null
                                }
                                <PostFooter likes={likes} dislikes={dislikes} comments={comments} liked={liked} />
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </Surface>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        selectForum: (forum)=>dispatch(selectForum(forum)),
    }
}

const mapStateToProps = (state) => {
    return {
        // user: state.user,
        forumSelected: state.forumSelected,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ForumComponent);

class PostFooter extends React.Component{
    render(){
        return (
            <View style={{backgroundColor: Colors.white, flexDirection: 'row', alignItems: 'center'}}>
                {/* <Button icon="thumb-up" style={{width: '50%'}} mode="text" onPress={() => console.log('Pressed')}>
                    Like
                </Button>
                <Button icon="comment" style={{width: '50%'}} mode="text" onPress={() => console.log('Pressed')}>
                    Comment
                </Button> */}
                
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <IconButton
                        icon="thumb-up"
                        color={this.props.liked==1?Colors.green500:Colors.grey500}
                        size={16}
                        onPress={() => console.log('this.like()')}
                    />
                    <Caption style={{left: -5, color:Colors.grey500}}>
                        {this.props.likes}
                    </Caption>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <IconButton
                        icon="thumb-down"
                        color={this.props.liked==0?Colors.red500:Colors.grey500}
                        size={16}
                        onPress={() => console.log('this.dislike()')}
                    />
                    <Caption style={{left: -5, color:Colors.grey500}}>
                        {this.props.dislikes}
                    </Caption>
                </View>
                <View style={{marginLeft: 'auto', flexDirection: 'row', alignItems: 'center'}}>
                    <IconButton
                        color={Colors.grey500}
                        icon="comment"
                        size={16}
                    />
                    <Caption style={{left: -5, color:Colors.grey500}}>
                        {this.props.comments}
                    </Caption>
                </View>
            </View>
        )
    }
}
class PostHeader extends React.Component{
    renderSeparator(){
        return(
            <View style={styles.separator}></View>
        )
    }
    render(){
        const {fullName} = this.props.user;
        return(
            <View style={styles.header}>
                <Text style={{...material.subheading, ...robotoWeights.medium}}>
                    {fullName}
                </Text>
                <MaterialIcons color={Colors.blue500} name="verified-user" size={16} style={{marginLeft: 2}} />                            
                {this.renderSeparator()}
                <Text style={{...material.subheading, ...robotoWeights.light}}>
                    {new Date(this.props.date.seconds*1000).getHours()}
                    :
                    {new Date(this.props.date.seconds*1000).getMinutes()}
                </Text>
                {/* {this.renderSeparator()}
                <Text style={{...material.subheading, fontSize: 12,}}>
                    travel
                </Text> */}
                {/* <IconButton
                    icon="more-vert"
                    color={Colors.blue500}
                    size={16}
                    style={{height:14, margin:0, marginLeft: 'auto'}}
                    onPress={() => console.log('Pressed')}
                /> */}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row', 
        paddingTop: 16, 
        // paddingTop: 5, 
        // paddingBottom: 2,
    },
    leftSide: {
        // paddingTop: 10, 
        width: 70, 
        alignItems: "center",
    },
    rightSide: {
        // marginLeft: 6, 
        flex: 1, 
        flexWrap: 'wrap',
        paddingRight: 16,
    },
    header: {
        flexDirection: 'row', 
        alignItems: 'center',
    },
    separator: {
        width: 2, 
        height: 2, 
        borderRadius: 1,
        backgroundColor: Colors.grey300, 
        marginHorizontal: 4
    }
})

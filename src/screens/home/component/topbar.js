import React from "react";
import { View, StatusBar } from "react-native";

// Custom Components
import { Colors, Surface, Banner, IconButton, Title, TouchableRipple } from "react-native-paper";

// Translate
import { translate } from "../../../functions/translate";
import { StackActions } from "react-navigation";

export default class TopBarComponent extends React.Component {
    
    constructor(props) {
        super(props);
    }

    changeRoute = (routeName)=>{
        const pushAction = StackActions.push({
            routeName
        });
        
        this.props.navigation.dispatch(pushAction);
    }

    render() {
        return (
            <View>
                <StatusBar backgroundColor={Colors.grey300} barStyle="dark-content" />                
                <Surface style={{height: 56, backgroundColor: '#007dc6', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', elevation: 0, zIndex: 999}}>
                    <IconButton
                        icon="map"
                        color={Colors.white}
                        size={24}
                        onPress={()=>this.changeRoute('LocationSelect')}
                    />
                    <TouchableRipple onPress={()=>this.changeRoute('SelectCity')}>
                        <Title numberOfLines={1} style={{margin: 0, color: Colors.white}}>{this.props.user.selectedCity || this.props.user.selectedCountryName}</Title>
                    </TouchableRipple>
                    <IconButton
                        icon="sort"
                        color={Colors.white}
                        size={24}
                        onPress={()=>this.props.navigation.openDrawer()}
                    />
                </Surface>
                <Banner
                    visible={this.props.user.notify}
                    actions={[{ label: translate('actions.close'), onPress: this.props.closeBanner }]}
                >{this.props.user.notifyContent}</Banner>
            </View>
        );
    }
}
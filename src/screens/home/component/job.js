import React from "react";
import {
  View
} from "react-native";
import { material } from 'react-native-typography';
import { Button, Card, Title, Paragraph, Divider, Subheading, Caption, IconButton } from 'react-native-paper';
import { withPreventDoubleClick } from "../../../components/preventDoubleClick";
import { CardTitleComponent } from "./card-title";



export default class JobComponent extends React.Component {
    constructor(props){
        super(props);

        this.state = {}
    }
    
    render() {
        return (
            <Card style={[{marginBottom: 10, }]}>
                {/* <CardTitleComponent />                 */}
                
                <Card.Content>
                    <Title>Менеджер по продажам</Title>
                    <Subheading>от 30 000 до 40 000 руб. до вычета НДФЛ</Subheading>
                    <Paragraph numberOfLines={2}>
                        Офис продаж – «визитная карточка» нашего бизнеса! И ты можешь стать частью передовой команды!
                    </Paragraph>
                    <Caption>Владивосток, Алеутская улица, 28</Caption>
                </Card.Content>
                
                <Card.Actions>
                    <Button style={{marginRight: 'auto'}} icon={'attach-file'} mode="text" onPress={() => alert('Apply!')}>
                        Откликнуться
                    </Button>
                    <IconButton
                        icon="favorite-border"
                        size={20}
                        onPress={() => console.log('Pressed')}
                    />
                    <IconButton
                        icon="call"
                        size={20}
                        onPress={() => console.log('Pressed')}
                    />
                </Card.Actions>
            </Card>
        )
    }
}
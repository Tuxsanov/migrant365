import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
} from 'react-native';
import { Divider, Colors, List, Checkbox, Appbar, Searchbar } from 'react-native-paper';
import { changeRoute } from '../../../functions/route';
import { NavigationActions } from 'react-navigation';

export default class ForumCategoriesPage extends Component {
  
    state = {
        isSwitchOn: false,
        checked: true,
        firstQuery: '',
        categories: [{
            title: 'All Categories'
        },{
            title: 'Location'
        },{
            title: 'Business'
        },{
            title: 'Nomads'
        },{
            title: 'Others'
        }]
    };


  render() {
    const { isSwitchOn, checked, firstQuery, categories } = this.state;
    return (
        <View style={{flex: 1}}>
            <Appbar.Header style={{backgroundColor: '#fff',}}>
                <Appbar.BackAction onPress={()=>(
                    this.props.navigation.dispatch(NavigationActions.back())
                )} />
                <Appbar.Content
                    title="Categories"
                />
                <Appbar.Action icon="search" />
            </Appbar.Header>
            <Divider />
            <View>
                <Searchbar
                    style={{elevation: 0, backgroundColor: Colors.grey200,}}
                    placeholder="Search"
                    onChangeText={query => { this.setState({ firstQuery: query }); }}
                    value={firstQuery}
                />
                <Divider />
            </View>
            <ScrollView style={styles.container}>
                
                <List.Section style={{backgroundColor: '#fff',}}>
                    {
                        categories.map(category=>(
                            <List.Item
                                key={category.title}
                                title={category.title.toUpperCase()}
                                titleStyle={[{color: Colors.grey500}]}
                                right={() => 
                                    <Checkbox
                                        status={checked ? 'checked' : 'unchecked'}
                                        onPress={() => { this.setState({ checked: !checked }); }}
                                    />
                                }
                            />
                        ))
                    }
                </List.Section>
            
            </ScrollView>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // padding: 20,
  },
});
import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
} from 'react-native';
import { List, Appbar, RadioButton } from 'react-native-paper';
import { NavigationActions } from 'react-navigation';

export default class ForumFilterPage extends Component {
  
state  = {
    isSwitchOn: false,
    checked: true,
    firstQuery: '',
    categories: [{
        title: 'All Categories'
    },{
        title: 'Location'
    },{
        title: 'Business'
    },{
        title: 'Nomads'
    },{
        title: 'Others'
    }]
  };


  render() {
    const { isSwitchOn, checked, firstQuery, categories } = this.state;
    return (
        <View style={{flex: 1}}>
            <Appbar.Header style={{backgroundColor: '#fff',}}>
                <Appbar.BackAction onPress={()=>(
                    this.props.navigation.dispatch(NavigationActions.back())
                )} />
                <Appbar.Content
                    title="Filter"
                />
            </Appbar.Header>
            {/* <Divider /> */}
            <ScrollView style={styles.container}>

                <List.Section style={{backgroundColor: '#fff',}}>
                    <List.Subheader>Order by:</List.Subheader>
                    <List.Item
                        title="Date of publish"
                        // description="132 people"
                        onPress={() => { this.setState({ checked: 'first' }); }}                        
                        right={()=>(
                            <RadioButton
                                value="first"
                                onPress={() => { this.setState({ checked: 'first' }); }}
                                status={checked === 'first' ? 'checked' : 'unchecked'}
                            />
                        )}
                    />
                    <List.Item
                        title="Most liked"
                        onPress={() => { this.setState({ checked: 'second' }); }}
                        right={()=>(
                            <RadioButton
                                value="second"
                                onPress={() => { this.setState({ checked: 'second' }); }}
                                status={checked === 'second' ? 'checked' : 'unchecked'}
                            />
                        )}
                    />
                    <List.Item
                        title="Verified users"
                        onPress={() => { this.setState({ checked: 'third' }); }}
                        right={()=>(
                            <RadioButton
                                value="third"
                                onPress={() => { this.setState({ checked: 'third' }); }}
                                status={checked === 'third' ? 'checked' : 'unchecked'}
                            />
                        )}
                    />
                </List.Section>
            
            </ScrollView>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f6f9',
    // marginTop: 10,
    // padding: 20,
    paddingTop: 8,
  },
});
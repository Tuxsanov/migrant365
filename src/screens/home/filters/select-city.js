import React, { Component } from 'react';
import { Text, View, FlatList } from 'react-native';
import MGToolbarBack from '../../../components/toolbar-back';
import { translate } from '../../../functions/translate';
import { List, Divider, Colors } from 'react-native-paper';

// Firebase
import firebase from 'react-native-firebase';

// Redux
import { connect } from 'react-redux';
import { updateUser } from '../../../../redux/actions/user';
import { NavigationActions } from 'react-navigation';
import { material } from 'react-native-typography';
import { defer, forkJoin } from 'rxjs';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class SelectCityScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            regions: []
        }
    }

    selectCity = (city)=>{
        this.props.updateUser({selectedCity: city.name});
        firebase.firestore().collection('user').doc(this.props.user.uid)
        .set({
            selectedCity: city.name
        }, {
            merge: true
        }).then(()=>{
            this.props.navigation.dispatch(NavigationActions.back());
        }).catch(err=>alert(err));
    }

    getRegions(){
        // if(!this.props.user.selectedCountry){return}
        this.setState({loading: true});
        this.forkJoin = forkJoin([
            firebase.firestore().collection('region')
            .doc(this.props.user.selectedCountry || 'uz')
            .get()
        ]);
    }

    componentDidMount(){
        this.getRegions();

        this.subcription = this.forkJoin.subscribe(res=>{
            // alert(res);
            if(res[0].exists){
                this.setState({loading: false, regions: res[0].data().regions});
            }
        })
    }

    renderList = ({item})=>(<List.Item 
        right={(props)=><MaterialIcons size={20} style={{alignSelf: 'center'}} name="check" />}
        onPress={()=>this.selectCity(item)} 
        titleStyle={[material.button]}
        title={item.name} />);

    render() {
        return (
            <View style={{flex:1}}>
                <MGToolbarBack title={translate('selectCity.title')} navigation={this.props.navigation} />
                <FlatList
                    onRefresh={()=>this.getRegions()}
                    refreshing={this.state.loading}

                    initialNumToRender={this.state.regions.length}

                    data={this.state.regions}
                    keyExtractor={(item, index) => item.name}
                    renderItem={this.renderList}
                    ItemSeparatorComponent={()=><Divider />}
                />
            </View>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateUser: (user)=>dispatch(updateUser(user)),
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectCityScreen);
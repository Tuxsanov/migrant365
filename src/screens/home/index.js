import React from "react";
import { StyleSheet, View } from "react-native";
import ScrollableTabView from 'react-native-scrollable-tab-view';
import AsyncStorage from '@react-native-community/async-storage';

// Custom Components
import ScrollableTabBar from "../../../ScrollableTabBar";
import { Colors } from "react-native-paper";


// Tabs
import ForumScreen from "../forum";
import HomeTabEvent from "./tab/event";
import HomeTabJob from "./tab/job";
import HomeTabFlight from "./tab/flight";

// Redux
import { connect } from 'react-redux';

// React
import firebase from "react-native-firebase";

// Topbar
import TopBarComponent from "./component/topbar";
import { updateUser } from "../../../redux/actions/user";

class HomeScreen extends React.Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
            tab: null,
        }
    }

    async componentDidMount(){
        const uid = await AsyncStorage.getItem('uid');

        let homeTab;
        if(uid){
            homeTab = await firebase.firestore().collection('user').doc(uid).collection('menu').doc('homeTab').get();
        }else{
            homeTab = await firebase.firestore().collection('menu').doc('homeTab').get();
        }
        
        this.setState({tab:homeTab.data()});
    }

    closeBanner = async ()=>{
        this.props.updateUser({
            notify: false
        });

        const uid = await AsyncStorage.getItem('uid');
        await firebase.firestore().collection('user').doc(uid).set({notify: false}, {merge: true});
    }

    render() {
        return (
            <View style={styles.container}>
                <TopBarComponent user={this.props.user} closeBanner={this.closeBanner} navigation={this.props.navigation} />
                {
                    (this.state.tab&&this.props.user)&&
                    <ScrollableTabView
                        tabBarBackgroundColor={Colors.white}
                        tabBarUnderlineStyle={{borderBottomWidth: 2, borderBottomColor: '#007dc6',}}
                        renderTabBar={() => <ScrollableTabBar />}
                    >
                        <ForumScreen
                            tabLabel={JSON.stringify(this.state.tab.forum)}
                            user={this.props.user}
                            navigation={this.props.navigation}
                        />
                        
                    
                        <HomeTabEvent
                            tabLabel={JSON.stringify(this.state.tab.event)}
                            user={this.props.user}
                            navigation={this.props.navigation}
                        />
                        
                        <HomeTabJob
                            tabLabel={JSON.stringify(this.state.tab.job)}
                            user={this.props.user}
                            navigation={this.props.navigation}
                        />

                        <HomeTabFlight
                            tabLabel={JSON.stringify(this.state.tab.airport_shuttle)}
                            user={this.props.user}
                            navigation={this.props.navigation}
                        />

                    </ScrollableTabView>
                }
                
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        updateUser: (user)=>dispatch(updateUser(user)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.grey300,
    },
})
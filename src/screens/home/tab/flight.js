import React, { Component } from 'react';
import { FlatList } from 'react-native';
import ListHeaderComponent from '../component/list-header';
import { keyExtractor } from '../functions/key-extractor';
import { homeStyles } from '../styles';
import { FlightComponent } from '../component/flight';

export default class HomeTabFlight extends Component {
    constructor(props){
        super(props);

        this.state = {
            data: [{
                type: "in",
                icon: "flight-land",
                to: "Moscow",
                from: "Bukhara",
                key: 'x5',
            },{
                type: "out",
                icon: "flight-takeoff",
                from: "Moscow",
                to: "Bukhara",
                key: 'v1',
            },{
                type: "out",
                icon: "local-taxi",
                from: "Moscow",
                to: "Bukhara",
                key: '2d',
            },{
                type: "in",
                icon: "local-taxi",
                from: "Tbilisi",
                to: "Moscow",
                key: '3s',
            }]
        }
    }
    
    // onEndReached = ()=>{
    //     this.setState({
    //         data: [...this.state.data, ...[{
    //             type: "in",
    //             icon: "flight-land",
    //             to: "Moscow",
    //             from: "Bukhara",
    //             key: '3s',
    //         }]]
    //     });
    // }

    renderItem = ({item, index})=>(
        <FlightComponent data={item} />
    );

    render(){
        const {data} = this.state;
        return (
            <FlatList
                onRefresh={()=>console.log('--- ')}
                refreshing={false}
                // onEndReached={this.onEndReached}

                ListHeaderComponent={<ListHeaderComponent />}

                onEndReachedThreshold={4}
                initialNumToRender={4}
                maxToRenderPerBatch={4}
                windowSize={10}

                data={data}
                style={homeStyles.flatList}
                renderItem={this.renderItem}
                keyExtractor={keyExtractor}
            />
        )
    }
}
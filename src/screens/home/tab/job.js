import React, { Component } from 'react';
import { FlatList } from 'react-native';
import JobComponent from '../component/job';
import { keyExtractor } from '../functions/key-extractor';
import ListHeaderComponent from '../component/list-header';
import { homeStyles } from '../styles';

// JOBS
export default class HomeTabJob extends Component {
    constructor(props){
        super(props);

        this.state = {
            data: [{
                title: "Группчане, кто может скинуть таблицу соответствия российских размеров колец турецким.Заранее благодарю.",
                key: 'sdf1',
            },{
                title: "Host fellow travelers to fund your own fun in the sun. Peak hosting season starts now.",
                imgURL: "https://picsum.photos/200/300",
                key: 'sdfsdf2',
            },{
                title: "Americaga Visa",
                imgURL: "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-9/56956523_2136511229766789_5071768047660826624_o.jpg?_nc_cat=111&_nc_ht=scontent-arn2-1.xx&oh=7f88002f76d6e290e1124d703a3ec6cc&oe=5D719AAD",
                key: 'sdfx3',
            }]
        }
    }
    // key = 3;
    // onEndReached = ()=>{
    //     let _data = this.state.data || [];
    //     this.key = this.key + 1;
    //     _data.push({
    //         key: this.key,
    //         title: "Host fellow travelers to fund your own fun in the sun. Peak hosting season starts now.",
    //         imgURL: "https://picsum.photos/id/"+this.key+"/200/300",
    //     });
    //     this.setState({
    //         data: _data
    //     });
    // }

    renderItem = ({item, index})=>(
        <JobComponent
            navigation={this.props.navigation}
            title={item.title}
            imgURL={item.imgURL}
        />
        
    )

    render() {
        const {data} = this.state;
        return (
            <FlatList
                onRefresh={()=>console.log('--- ')}
                // ListHeaderComponent={<ListHeaderComponent />}
                refreshing={false}
                // onEndReached={this.onEndReached}
                // onEndReachedThreshold={4}
                initialNumToRender={3}
                maxToRenderPerBatch={10}
                windowSize={10}
                
                data={data}
                style={homeStyles.flatList}
                renderItem={this.renderItem}
                keyExtractor={keyExtractor}
            />
        )
    }
}  
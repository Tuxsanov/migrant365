import React, { Component } from 'react';
import { ScrollView, View, FlatList } from 'react-native';
import { Subheader, Title, Card, List, Paragraph, Button, Appbar, IconButton, Text, Dialog, Portal, Searchbar, Colors } from 'react-native-paper';
import { keyExtractor } from '../functions/key-extractor';
import { homeStyles } from '../styles';
import { material } from 'react-native-typography';

import I18n from "../../../uitils/I18n";
import { PlacesCategoryData } from '../../../data/PlacesCategoryData';
import { changeRoute } from '../../../functions/route';
export default class HomeTabInfo extends Component {
    constructor(props){
        super(props);
        
        this.state = {
            data: [{id: '1', key: '1'}],
        }
    }

    componentDidMount(){
    }

    _renderItem = ({item, index})=>(
        <View style={{marginBottom: 10}}>
            {/* <Title>Explore</Title> */}
            <Searchbar
                style={{backgroundColor: Colors.grey800,}}
                icon={'place'}
                placeholder="Search"
            />
        </View>
        // <List.Item
        //     style={{backgroundColor: '#fff',}}
        //     title={I18n.t(`homeTabPlaces.list.${item.id}`, {locale: this.props.languageCode})}
        //     titleStyle={material.button}
        //     description="334 recomended places"
        //     onPress={()=>alert(`homeTabPlaces.list.${item.id}`)}
        //     left={(props) => <List.Icon {...props} icon={item.icon} />}
        //     right={(props) => <List.Icon {...props} icon="chevron-right" />}
        // />
    )

    render() {
        const {data} = this.state;
        const {refreshing, uuid, languageCode} = this.props;
        return (
            <FlatList
                onRefresh={()=>console.log('--- ')}
                // ListHeaderComponent={<ListHeaderComponent navigation={this.props.navigation} uuid={uuid} languageCode={languageCode} />}
                refreshing={refreshing}
                initialNumToRender={10}
                maxToRenderPerBatch={10}
                windowSize={21}
                data={data}
                // style={homeStyles.flatList}
                renderItem={this._renderItem}
                keyExtractor={keyExtractor}
            />
        )
    }
}

class ListHeaderComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    

    recomendPlace=()=>{
        if(!this.props.uuid){
            alert("Please sign in!")
        }else{
            changeRoute(this.props.navigation, 'RecommendPlaces');
        }
    }

    render() {
        return (
            <View>
                <Card style={{marginBottom: 10,}}>
                    <Card.Content>
                        <Paragraph>
                            {I18n.t('homeTabPlaces.info', {locale: this.props.languageCode})}
                        </Paragraph>
                    </Card.Content>
                    <Card.Actions style={{justifyContent: 'center'}}>
                        <Button icon="add-location" mode="text" onPress={()=>this.recomendPlace()}>
                            {I18n.t('actions.recommendPlace', {locale: this.props.languageCode})}
                        </Button>
                    </Card.Actions>
                </Card>
            </View>
        );
    }
}

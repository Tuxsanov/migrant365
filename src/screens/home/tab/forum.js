import * as React from 'react';
import { FlatList, View, Text, StyleSheet } from 'react-native';
import { Button, FAB, Portal, Divider, Avatar, Colors } from 'react-native-paper';
import ListHeaderComponent from '../component/list-header';
import { keyExtractor } from '../functions/key-extractor';
import ForumComponent from '../component/forum';
import { homeStyles } from '../styles';

// Firebase
import firebase from 'react-native-firebase';

import { material } from 'react-native-typography';
import { changeRoute } from '../../../functions/route';
// Redux
import { connect } from 'react-redux';
import MGToolbar from '../../../components/toolbar';

class HomeTabForum extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            open: false,
            visible: false,
            fetchingPosts: false,
            initialFetchingPosts: true,
            posts: [],
        }
    }

    getPosts(){
        this.setState({initialFetchingPosts: false, fetchingPosts: true})
        firebase.firestore()
        .collection('posts')
        .where('countries', 'array-contains', 'uz')
        // .orderBy('date', 'desc')
        // .limit(10)
        .get()
        .then(snapshot=>{
            let posts = [];
            snapshot.forEach(snap=>{
                posts.push({
                    id: snap.id,
                    ...snap.data()
                });
            });
            this.setState({posts, fetchingPosts: false})
        })
    }

    componentWillMount(){
    }

    componentDidMount(){
        this.getPosts();
    }

    componentDidUpdate(prevProps) {
        if (this.props.user.selectedCity !== prevProps.user.selectedCity) {
            this.getPosts();
        }
      }

    renderItem = ({item, index})=>(
        item?
        <ForumComponent forum={item} navigation={this.props.navigation} />
        :null
    );

    navigate = (navScreen)=>{
        changeRoute(this.props.navigation, navScreen);
    }

    render() {
        const {tabLabel, navigation, loading, user} = this.props;
        const {posts, fetchingPosts} = this.state;
        return (
            <View style={{flex: 1}}>
                <MGToolbar 
                    navigation={this.props.navigation} 
                    user={this.props.user} 
                />
                <FlatList
                    ListHeaderComponent={
                        <ListHeaderComponent 
                            user={user} 
                            navigation={navigation}
                            selectedCountry={this.props.user.selectedCountry} 
                        />
                    }
                    ListFooterComponent={<ListFooterComponent />}
                    onRefresh={()=>this.getPosts()}
                    refreshing={loading || fetchingPosts}
                    // onEndReached={this.onEndReached}
                    
                    // onEndReachedThreshold={4}
                    initialNumToRender={5}
                    maxToRenderPerBatch={10}
                    windowSize={10}
    
                    tabLabel={tabLabel}
                    data={posts}
                    // ItemSeparatorComponent={()=><Divider />}
                    style={homeStyles.flatList}
                    renderItem={this.renderItem}
                    // contentContainerStyle={{padding: 10}}
                    keyExtractor={keyExtractor}
                />
                <FAB
                    style={styles.fab}
                    icon={()=><Avatar.Image 
                        size={24} 
                        style={{alignSelf: 'center',}} 
                        source={{uri: `https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${this.props.user.selectedCountry}.png`}} 
                    />}

                    small
                    label={this.props.user.selectedCountry}
                    onPress={() => this.navigate('LocationSelect')}
                />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};

export default connect(mapStateToProps)(HomeTabForum);

class ListFooterComponent extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <View style={{padding: 10, paddingTop:100, alignItems: 'center'}}>
                <Text style={[material.body1]}>
                    End list
                </Text>
                <Text style={[material.caption]}>
                    migrant365.com
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
        backgroundColor: Colors.white,
    },
})
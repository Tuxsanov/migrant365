import React, { Component } from 'react';
import { FlatList } from 'react-native';
import ListHeaderComponent from '../component/list-header';
import EventComponent from '../component/event';
import { keyExtractor } from '../functions/key-extractor';
import { homeStyles } from '../styles';

export default class HomeTabEvent extends Component {
    constructor(props){
        super(props);

        this.state = {
            data: [{
                id: 'sdf1',
                title: "Пятничный футбол с необходимостью 5 игроков",
                subtitle: "Поклонники всегда могут присутствовать))",
                attending: false,
            },{
                id: '2sdfsdf',
                title: "Собрались за плов",
                subtitle: "Почувствуй себя как дома",
                attending: true,
            },{
                id: '3sdsdfsdf',
                title: "Пятничный футбол с необходимостью 5 игроков",
                subtitle: "Поклонники всегда могут присутствовать))",
                attending: false,
            }]
        }
    }
    // key = 3;
    // onEndReached = ()=>{
    //     let _data = this.state.data || [];
    //     this.key = this.key + 1;
    //     _data.push({
    //         key: 3,
    //         title: "Пятничный футбол с необходимостью 5 игроков",
    //         subtitle: "Поклонники всегда могут присутствовать))",
    //         attending: false,
    //     });
    //     this.setState({
    //         data: _data
    //     });
    // }

    renderItem = ({item, index})=>(
        <EventComponent item={item} />
    )

    render() {
        const {tabLabel, navigation, loading, user} = this.props;
        const {data} = this.state;
        return (
            <FlatList
                ListHeaderComponent={<ListHeaderComponent user={user} navigation={navigation} />}
                onRefresh={()=>console.log('--- ')}
                refreshing={false}
                // onEndReached={this.onEndReached}
                // onEndReachedThreshold={4}
                initialNumToRender={3}
                maxToRenderPerBatch={10}
                windowSize={10}
                data={data}
                style={homeStyles.flatList}
                renderItem={this.renderItem}
                keyExtractor={keyExtractor}
            />
        )
    }
}
import * as React from 'react';
import { View } from 'react-native';
import { Text, Button } from 'react-native-paper';
import { material } from 'react-native-typography';
import UserWrap from '../components/userWrap';
import ChatScreen from './chat';


const ContentInfo = ()=>{
    return (
        <View style={{paddingHorizontal: 15, marginBottom: 10}}>
            <Text style={[material.button, {marginBottom: 5,}]}>
                Группчане, кто может скинуть таблицу соответствия российских размеров колец турецким.
            </Text>
            <Text style={[material.caption, {marginVertical: 5}]}>
                12 Oct 2018  12:00 pm
            </Text>
            <UserWrap />
        </View>
    )
}

export default class ForumDetailScreen extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            info: true,
        }
    }

    toggleInfo = ()=>{
        this.setState({info: !this.state.info});
    }
    
    render() {
        const {info} = this.state;
        return (
            <View style={{flex: 1}}>
                <View style={{flexDirection: 'column', backgroundColor: '#fff', paddingVertical: info?15:0}}>
                    {info?<ContentInfo />:null}
                    <Button icon={`keyboard-arrow-${info?'up':'down'}`} style={{borderRadius: 0}} mode="contained" onPress={() => this.toggleInfo()}>
                        {info?'Hide':'Show'}
                    </Button>
                </View>
                <ChatScreen />
            </View>

        );
    }
}
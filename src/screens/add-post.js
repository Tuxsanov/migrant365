import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { material } from 'react-native-typography';
import { Button, TextInput } from 'react-native-paper';

import { StackActions } from 'react-navigation';
import MGToolbar from '../components/toolbar';

const pushAction = StackActions.push({
  routeName: 'PostAdd',
//   params: {
//     myUserId: 9,
//   },
});


export default class AddPostScreen extends Component {
    state = {
        text: ''
    };

    changeRoute=()=>{
        this.props.navigation.dispatch(pushAction);
    }

    render() {
        return (
            <View style={{flex:1}} >
                {/* <MGToolbar /> */}
                <View style={{flex:1,justifyContent: 'center', paddingHorizontal: 40}}>
                    <Text style={material.headline}>
                        Creating a Post Costs 
                        $1 US Dollar in Order to
                        Avoid Low Post Quality
                    </Text>
                    <Text style={material.body1}>
                        * Google Pay
                    </Text>
                    <Text style={material.body1}>
                        * No refund available
                    </Text>
                    <Text style={material.body1}>
                        * Before posting it will be checked by moderator
                    </Text>
                    <Button onPress={()=>this.changeRoute()} style={{marginTop: 15,}} mode="contained" icon="add">
                        Agree & Create
                    </Button>
                </View>
            </View>
        )
    }
}

import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { material, robotoWeights } from 'react-native-typography';
import { List, Avatar, Button, TextInput, Title } from 'react-native-paper';

import { StackActions } from 'react-navigation';
import MGToolbar from '../components/toolbar';

const pushAction = StackActions.push({
  routeName: 'PostAdd',
});


export default class NotificationsScreen extends Component {
    state = {
        text: '',
    };

    changeRoute=()=>{
        this.props.navigation.dispatch(pushAction);
    }

    render() {
        return (
            <ScrollView style={{flex:1}}>
                <View style={{paddingHorizontal: 15}}>
                    <Title>No notifications yet</Title>
                </View>
            </ScrollView>
        )
    }
}

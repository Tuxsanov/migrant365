import React, { Component } from 'react';
import { ScrollView, View, Text } from 'react-native';
import { List, Colors, Appbar, Searchbar, Divider, ActivityIndicator } from 'react-native-paper';
import { iOSColors, robotoWeights } from 'react-native-typography';
import { debounce } from 'lodash'


import firebase from 'react-native-firebase';

export default class CountryListScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstQuery: '',
            cityList: [],
            _cityList: [],
            loading: true,
        };

    }

    componentDidMount(){
        firebase.firestore().collection("country").onSnapshot((querySnapshot)=> {
            let cityList = [];
            querySnapshot.forEach((doc)=> {
                let country = doc.data();
                cityList.push(country);
            });

            this.setState({loading: false});
            this.setState({cityList});
            this.setState({_cityList:cityList});
        });
    }

    searchCity = (query)=>{
        this.setState({ firstQuery: query });
        let cityList = JSON.parse(JSON.stringify(this.state._cityList));
        if(query){
            cityList = cityList.filter(country=>(country.country).toLocaleLowerCase().startsWith(query.toLocaleLowerCase()))
            this.setState({cityList});
        }else{
            this.setState({cityList});
        }
    }

    render() {
        const {loading, firstQuery, cityList} = this.state;
        return (
            <View style={{flex: 1}}>
                <Appbar.Header style={{backgroundColor: '#fff',}}>
                    <Appbar.BackAction onPress={()=>(
                        this.props.navigation.dispatch(NavigationActions.back())
                    )} />
                    <Appbar.Content
                        title="Select Country"
                    />
                </Appbar.Header>
                {
                    !loading?
                    <View>
                        <Divider />
                        <Searchbar
                            style={{elevation: 0}}
                            placeholder="Search"
                            onChangeText={debounce(this.searchCity, 40)}
                        />
                    </View>
                    :null
                }
                {
                    loading?
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <ActivityIndicator animating={true} color={Colors.red800} />
                    </View>:
                    <ScrollView style={{backgroundColor: Colors.white}}>
                        <List.Section>
                            <List.Subheader>Select Countries</List.Subheader>
                            {
                                cityList.map((country,index)=>(
                                    <List.Item
                                        key={index}
                                        title={country.country}
                                        right={() => <List.Icon icon="check-circle" />}
                                    />
                                ))
                            }
                        </List.Section>
                    </ScrollView>
                }
            </View>
        );
    }
}

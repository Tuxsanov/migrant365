import * as React from 'react';
import { FlatList, TouchableOpacity, Image, View, Text, StyleSheet } from 'react-native';
import { Button, FAB, Portal, Divider, Avatar, IconButton, Colors, Surface, Appbar } from 'react-native-paper';
// import ListHeaderComponent from '../component/list-header';
import ForumComponent from '../home/component/forum';
// import { homeStyles } from '../styles';

// Firebase
import firebase from 'react-native-firebase';

// Redux
import { connect } from 'react-redux';
import { keyExtractor } from '../home/functions/key-extractor';
import ListEndComponent from '../../components/list-end';
import ForumHeaderComponent from './header';

class ForumScreen extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            open: false,
            visible: false,
            fetchingPosts: false,
            initialFetchingPosts: true,
            posts: [],
        }
    }

    getPosts(){
        if(!this.props.user.selectedCountry){return}
        
        this.setState({posts: [], initialFetchingPosts: false, fetchingPosts: true})
        
        
        let filterBy = 'countries';
        let filterVal = this.props.user.selectedCountry;
        
        if(this.props.user.selectedCity){
            filterBy = 'cities';
            filterVal = this.props.user.selectedCity;
        }

        firebase.firestore().collection('posts')
        .where(filterBy, 'array-contains', filterVal)
        .limit(10).get()
        .then(snapshot=>{
            let posts = [];
            snapshot.forEach(snap=>{
                posts.push({
                    id: snap.id,
                    ...snap.data()
                });
            });
            this.setState({posts, fetchingPosts: false})
        })
    }

    componentWillMount(){
    }

    componentDidMount(){
        this.getPosts();
    }

    componentDidUpdate(prevProps) {
        if (this.props.user.selectedCity !== prevProps.user.selectedCity) {
            this.getPosts();
        }

        if (this.props.user.selectedCountry !== prevProps.user.selectedCountry) {
            this.getPosts();
        }
      }

    renderItem = ({item, index})=>(
        item?
        <ForumComponent forum={item} navigation={this.props.navigation} />
        :null
    );

    render() {
        const {posts, fetchingPosts} = this.state;
        return (
            <View style={{flex: 1}}>
                <FlatList
                    ListHeaderComponent={<ForumHeaderComponent user={this.props.user} navigation={this.props.navigation} />}
                    ListFooterComponent={<ListEndComponent />}
                    onRefresh={()=>this.getPosts()}
                    refreshing={this.props.loading || fetchingPosts}
                    
                    initialNumToRender={5}
                    maxToRenderPerBatch={10}
                    windowSize={10}
    
                    tabLabel={this.props.tabLabel}
                    data={posts}
                    style={{backgroundColor: Colors.grey300}}
                    renderItem={this.renderItem}
                    keyExtractor={keyExtractor}
                />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};

export default connect(mapStateToProps)(ForumScreen);
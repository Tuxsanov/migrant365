import * as React from 'react';
import { View, Text } from 'react-native';
import { material } from 'react-native-typography';
import { Surface, Avatar, Colors, TouchableRipple } from 'react-native-paper';
import { StackActions } from 'react-navigation';
export default class ForumHeaderComponent extends React.Component{
    constructor(props){
        super(props)
    }

    changeRoute = (routeName)=>{
        const pushAction = StackActions.push({
            routeName
        });
        
        this.props.navigation.dispatch(pushAction);
    }

    render(){
        return(
            <Surface style={{elevation: 1}}>
                <TouchableRipple onPress={()=>this.changeRoute('PostAdd')}>
                    <View style={{paddingVertical: 16, flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{width: 70, justifyContent: 'center', alignItems: 'center'}}>
                            <Avatar.Image size={35} source={{uri: 'https://randomuser.me/api/portraits/women/68.jpg'}} />
                        </View>
                        <Text style={[material.subheading, {color: Colors.grey400}]}>
                            Ask help from community
                        </Text>
                    </View>
                </TouchableRipple>
            </Surface>
        )
    }
}
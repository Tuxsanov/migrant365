import React, {Component} from 'react';
import { Platform, View, Text } from 'react-native';
import { Appbar, Colors, Divider, IconButton, Caption } from 'react-native-paper';
// import PropTypes from 'prop-types';
import { GiftedChat, Actions, SystemMessage, Send } from 'react-native-gifted-chat';
// import emojiUtils from 'emoji-utils';

// Redux
import { connect } from 'react-redux';

// Firebase
import firebase from 'react-native-firebase';

import ChatDesign from './chatDesign';

class ChatScreen extends React.Component {
  state = {
    messages: [],
  }

  componentDidMount() {
    // this.setState({
    //   messages: [
    //     {
    //       _id: 1,
    //       text: 'Hello developer',
    //       createdAt: new Date(),
    //       user: {
    //         _id: 2,
    //         name: 'React Native',
    //         avatar: 'https://placeimg.com/140/140/any',
    //       },
    //     },
    //   ],
    // });

    firebase.firestore()
    .collection('posts')
    .doc('zYztGjZVTTuFd6QJOkuR')
    .collection('chat')
    .orderBy('createdAt', 'asc')
    .onSnapshot((snapshot)=> {
      snapshot.docChanges.forEach((change)=> {
          if (change.type === "added") {
            let data = change.doc.data();
            this.setState(previousState => {
              return {
                messages: GiftedChat.append(previousState.messages, [data]),
              }
            });
          }
          if (change.type === "modified") {
              // console.log("Modified city: ", change.doc.data());
          }
          if (change.type === "removed") {
              // console.log("Removed city: ", change.doc.data());
          }
      });
    });
  }

  componentWillMount() {
  }

  onSend(messages = []) {
    // this.setState(previousState => ({
    //   messages: GiftedChat.append(previousState.messages, messages),
    // }))
    let data = messages[0];
    data.createdAt = + new Date();
    firebase.firestore()
    .collection('posts')
    .doc('zYztGjZVTTuFd6QJOkuR')
    .collection('chat')
    .add(data)
    .then()
    .catch(e=>alert(e));
  }

  renderMessage(props) {
    const { currentMessage: { text: currText } } = props;

    let messageTextStyle;

    // Make "pure emoji" messages much bigger than plain text.
    // if (currText) {
    //   // emojiUtils.isPureEmojiString(currText)
    //   messageTextStyle = {
    //     fontSize: 28,
    //     // Emoji get clipped if lineHeight isn't increased; make it consistent across platforms.
    //     lineHeight: Platform.OS === 'android' ? 34 : 30,
    //   };
    // }

    return (
      <ChatDesign {...props} messageTextStyle={messageTextStyle} />
    );
  }

  renderSend(props) {
    return (
        <Send
            {...props}
        >
            <View style={{marginRight: 10, marginBottom: 5}}>
                {/* <Image source={require('../../assets/send.png')} resizeMode={'center'}/> */}
              <IconButton
                icon="add-a-photo"
                color={Colors.red500}
                size={20}
                onPress={() => console.log('Pressed')}
              />
            </View>
        </Send>
    );
  }

  render() {
    return (
      <View style={{flex:1}}>
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          // renderSend={this.renderSend}
          // renderInputToolbar={<RenderInputToolbar />}
          minComposerHeight={44}
          // dateFormat={}
          
          user={{
            uid: 'zHKN9R5o5FeXYwUzY68ykI8FrjI3',
            avatar: 'https://firebasestorage.googleapis.com/v0/b/migrant365com.appspot.com/o/avatars%2FzHKN9R5o5FeXYwUzY68ykI8FrjI3?alt=media&token=ff625a36-d6d7-4099-bba4-0a571774ffdc',
            name: 'Jane Doe',
          }}
          // renderMessage={this.renderMessage}
          // renderMessageText={this.renderMessageText}
        />
      </View>
    );
  }

}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};
  
export default connect(mapStateToProps)(ChatScreen);

class RenderInputToolbar extends Component{
  render(){
    return (
      <Text>Cool</Text>
    )
  }
}
import React, { Component } from 'react';
import { Image, ImageBackground, View, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Surface, Avatar, Text, IconButton, Appbar, FAB, TouchableRipple, Divider, Card, Paragraph, Title, ProgressBar, Headline } from 'react-native-paper';
import { material, iOSColors, robotoWeights } from 'react-native-typography';
import { StackActions } from 'react-navigation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import UserWrap from '../components/userWrap';

const pushAction = StackActions.push({
    routeName: 'PostDetails',
});

export default class EventDetailsComponent extends Component {
    constructor(props){
        super(props);

        this.state = {
            data: [{
                key: 1,
                title: "Пятничный футбол с необходимостью 5 игроков",
                subtitle: `Hello to all,
my name is Susie, I'm originally from Italy but now I live in Algarve. I'm a web designer and founder of Vivo in Portogallo
I've recently launched this​ ​website for the Italian community here in Portugal.
My mission​ ​for this website is to​ ​offer various services, show case events and create a bridge between the Italians living here and the Portuguese culture​ ​& traditions. A way for them to respect, integrate and participate with the local community.
I'm looking for local business owners that would be interested in adhering to my free fidelity card initiative. If you'd like more more info please write to me at info@vivoinportogallo.com
Obrigada :) `,
                attending: false,
            }]
        }
    }
    key = 3;
    onEndReached = ()=>{
        let _data = this.state.data || [];
        this.key = this.key + 1;
        _data.push({
            key: 3,
            title: "Пятничный футбол с необходимостью 5 игроков",
            subtitle: "Поклонники всегда могут присутствовать))",
            attending: false,
        });
        this.setState({
            data: _data
        });
    }

    _keyExtractor = (item, index) => index+'';

    renderItem = ({item, index})=>(
        <Card>
            <Card.Cover source={{ uri: 'https://picsum.photos/700' }} />
            <Card.Content>
                <View style={{flexDirection: 'row',}}>
                    <UserWrap />
                    <IconButton
                        icon="flag"
                        size={16}
                        style={{marginLeft: 'auto'}}
                        onPress={() => console.log('Pressed')}
                        />
                </View>
                
                <Headline>{item.title}</Headline>
                <Text style={[material.subheading, robotoWeights.bold]}>
                    Date/time 
                </Text>
                <Paragraph style={{marginBottom: 10,}}>Thu 29 Sep. 10:20-22:00</Paragraph>
                <Text style={[material.subheading, robotoWeights.bold]}>
                    Description
                </Text>
                <Paragraph style={{marginBottom: 10,}}>{item.subtitle}</Paragraph>
                <Text style={[material.subheading, robotoWeights.bold]}>
                    Attendees (12 left)
                </Text>
                <View style={{flexWrap: 'wrap', flexDirection: 'row', alignItems: 'center'}}>
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/male/19.jpg'}} />
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/female/9.jpg'}} />
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/male/9.jpg'}} />
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/male/2.jpg'}} />
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/female/3.jpg'}} />
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/female/1.jpg'}} />

                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/male/18.jpg'}} />
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/female/8.jpg'}} />
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/male/10.jpg'}} />
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/male/12.jpg'}} />
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/female/13.jpg'}} />
                    <Avatar.Image style={{marginBottom: 4, marginRight: 4}} size={36} source={{uri: 'https://uinames.com/api/photos/female/11.jpg'}} />
                </View>
            </Card.Content>
            <Card.Actions>
                <Button style={{width: '100%'}} mode={!item.attending?'contained':'outlined'} onPress={() => console.log('Pressed')}>
                    {!item.attending?'12$ / Apply':'Applied'}
                </Button>
            </Card.Actions>
        </Card>
    )

    ListHeaderComponent(){
        return (
            <Surface style={{backgroundColor: '#fff', padding: 15, flexDirection: 'row', }}>
                <View style={{borderColor: iOSColors.orange, borderRadius: 36, borderWidth: 2,}}>
                    <Avatar.Image size={36} source={{uri: 'https://uinames.com/api/photos/female/16.jpg'}} />
                </View>
                <View style={{justifyContent: 'center', backgroundColor: '#F5F5F5', paddingHorizontal: 12, borderWidth: 1, flex:1, borderRadius: 20, borderColor: iOSColors.lightGray, height: 38, marginLeft: 10}}>
                    <Text style={[material.button, {color: iOSColors.gray}]}>Создать событие</Text>
                </View>
            </Surface>
        )
    }

    render() {
        const {tabLabel, navigation} = this.props;
        const {data} = this.state;
        return (
            <FlatList
                onRefresh={()=>console.log('--- ')}
                // ListHeaderComponent={this.ListHeaderComponent}
                refreshing={false}
                // onEndReached={this.onEndReached}
                onEndReachedThreshold={4}
                initialNumToRender={4}
                maxToRenderPerBatch={10}
                windowSize={10}
                // tabLabel={tabLabel}
                data={data}
                style={{backgroundColor: '#f5f6f9'}}
                renderItem={this.renderItem}
                keyExtractor={this._keyExtractor}
            />
        )
    }
}

const styles = StyleSheet.create({
    
})
  
import React, { Component } from 'react';
import { CameraRoll, View, StyleSheet, Dimensions, TouchableOpacity, Button, ScrollView, Image, Text } from 'react-native';
// import AlbumsScreen from './album';
// import GalleryScreen from './gallery';
import { Surface, Colors, IconButton } from 'react-native-paper';
import { material } from 'react-native-typography';

const TOOLBAR_HEIGHT  = 56;
const ICON_SIZE = 24;

const TOOLBAR_ELEVATION = 4;

const CONTAINER_PADDING = 16;

const COLOR_PRIMARY = Colors.red500;

export default class ImageSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photos: []
        };
    }

    componentDidMount(){
        // this._handleButtonPress();
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Surface style={{...styles.row_center_hor, height: TOOLBAR_HEIGHT, elevation: TOOLBAR_ELEVATION}}>
                    
                    {/* CLOSE BTN */}
                    <View style={{width: 70}}>{
                        <IconButton
                            icon={'close'}
                            color={Colors.grey600}
                            size={24}
                            onPress={() => this.close()}
                        />
                    }</View>
                    
                    {/* SELECTING CITY */}
                    <TouchableOpacity activeOpacity={1} onPress={()=>this.changeRoute('SelectCity')}>
                        <View style={styles.row_center_hor}>
                            <Text style={[material.title]}>Select image</Text>
                        </View>
                    </TouchableOpacity>

                </Surface>
                {/* <AlbumsScreen /> */}
                {/* <GalleryScreen album={'Telegram'} /> */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    row_center_hor: {
        flexDirection: 'row',
        alignItems: 'center',
    }
})

import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {CameraKitGalleryView} from 'react-native-camera-kit';
import { updateUser } from '../../../redux/actions/user';

// Redux
import { connect } from 'react-redux';
import ImageSelectHeader from './header';
import { updateForm } from '../../../redux/actions/form';
import { StackActions, NavigationActions } from 'react-navigation';
import { IconButton, Colors, Surface } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { material } from 'react-native-typography';

// Customs
const TOOLBAR_HEIGHT  = 56;
const TOOLBAR_ELEVATION = 4;

class ImageGalleryScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      album: this.props.user.albumName,
      images: {},
      shouldRenderCameraScreen: false
    }
  }

  changeRoute = (routeName)=>{
    const resetAction = StackActions.reset({
      index: 1,
      actions: [
        NavigationActions.navigate({ routeName: 'DrawerTabNavigatorBottom' }),
        NavigationActions.navigate({ routeName })
      ],
    });
    
    this.props.navigation.dispatch(resetAction); 
  }

  close = ()=>(this.props.navigation.dispatch(NavigationActions.back()));

  render() {
    return (
      <View style={{flex: 1}}>
        {/* <ImageSelectHeader title={'Select image'} navigation={this.props.navigation} /> */}
        
        <Surface style={{...styles.row_center_hor, height: TOOLBAR_HEIGHT, elevation: TOOLBAR_ELEVATION}}>
          {/* BACK BTN */}
          <View style={{width: 70}}>{
            <IconButton
              icon={'arrow-back'}
              color={Colors.grey600}
              size={24}
              onPress={() => this.close()}
            />
          }</View>
          
          {/* SELECTING CITY */}
          <TouchableOpacity activeOpacity={1} onPress={()=>this.changeRoute('SelectCity')}>
            <View style={styles.row_center_hor}>
              <Text style={[material.title]}>{'Select image'}</Text>
            </View>
          </TouchableOpacity>

          <IconButton
            style={{marginLeft: 'auto'}}
            icon={'check'}
            color={Colors.red600}
            size={24}
            onPress={()=>this.changeRoute('PostAdd')}
          />

        </Surface>

        <CameraKitGalleryView
          ref={(gallery) => {
            this.gallery = gallery;
          }}
          style={{flex:1, backgroundColor: '#ffffff',}}
          albumName={this.state.album}
          minimumInteritemSpacing={4}
          minimumLineSpacing={4}
          columnCount={3}
          selectedImages={Object.keys(this.state.images)}
          onTapImage={this.onTapImage.bind(this)}
          selection={{
            selectedImage: require('./selected.png'),
            imagePosition: 'bottom-right',
            imageSizeAndroid: 'medium',
            enable: (Object.keys(this.state.images).length < 5)
          }}
        />
      </View>
    )
  }
  
  onTapImage(event) {
    const uri = event.nativeEvent.selected;
    if (this.state.images[uri]) {
      delete this.state.images[uri];
    } else {
      this.state.images[uri] = true;
    }
    this.setState({images: {...this.state.images}})
    this.props.updateForm({
      // selectedFormImgs: [uri] 
      selectedFormImgs: Object.keys(this.state.images) 
    });
  }
}


const mapDispatchToProps = dispatch => {
  return {
    updateForm: (form)=>dispatch(updateForm(form)),
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageGalleryScreen);

const styles = StyleSheet.create({
  row_center_hor: {
    flexDirection: 'row',
    alignItems: 'center',
  }
});
import React, { Component } from 'react';
import { View, FlatList, TouchableOpacity, Image, Text } from 'react-native';
import { CameraKitGallery } from 'react-native-camera-kit';
import { Colors, IconButton } from 'react-native-paper';
import { material } from 'react-native-typography';
import { updateUser } from '../../../redux/actions/user';

// Redux
import { connect } from 'react-redux';
import { StackActions } from 'react-navigation';
import ImageSelectHeader from './header';

class ImageAlbumsScreen extends Component {
  constructor(props) {

    super(props);
    this.state = {
      albumsDS: []
    }
  }

  componentDidMount() {
    this.onGetAlbumsPressed();
  }

  changeRoute = (routeName, albumName)=>{
    this.props.updateUser({
      albumName
    })
    const pushAction = StackActions.push({
      routeName
    });
    
    this.props.navigation.dispatch(pushAction);
  }

  _renderRow = ({item, index})=>{
    const image = 'file://' + item.thumbUri;
    return (
      <TouchableOpacity 
        activeOpacity={1} 
        style={{flex:1, backgroundColor: Colors.white, paddingVertical: 8, flexDirection: 'row' }}
        onPress={()=>this.changeRoute('ImageGallery', item.albumName)}
      >
        <View style={{width: 70, alignItems: 'center'}}>
          <Image
            style={{width: 50, height: 50, borderRadius: 4}}
            source={{uri: image}} 
          />
        </View>
        <View 
          style={{flexDirection: 'row', flex: 1, alignItems: 'center',}} 
        >
          <View style={{flex: 1, flexWrap: 'wrap'}}>
            <Text style={[material.subheading, {color: Colors.grey800}]}>{item.albumName}</Text>
            <Text style={[material.caption]}>{item.imagesCount}</Text>
          </View>
          <IconButton
            icon="chevron-right"
            color={Colors.grey800}
            size={24}
            style={{marginLeft: 'auto'}}
            onPress={() => console.log('Pressed')}
          />
        </View>
      </TouchableOpacity>

    )
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ImageSelectHeader title={'Image folder'} navigation={this.props.navigation} />
        <FlatList
          style={{backgroundColor: Colors.grey300}}
          data={this.state.albumsDS}
          renderItem={this._renderRow}
          keyExtractor={item => item.albumName}
        />
      </View>
    );
  }

  async onGetAlbumsPressed() {
    let albums = await CameraKitGallery.getAlbumsWithThumbnails();
    albums = albums.albums;

    this.setState({albumsDS: albums});
  }

}

const mapDispatchToProps = dispatch => {
  return {
    updateUser: (user)=>dispatch(updateUser(user)),
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageAlbumsScreen);
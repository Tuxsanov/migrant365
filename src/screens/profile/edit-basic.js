import React, { Component } from 'react';
import { View, Text, ScrollView, Image, Picker, ProgressBarAndroid, RefreshControl, } from 'react-native';
import { List, Appbar, Colors, TextInput, Divider, Button, Snackbar, ActivityIndicator, Avatar, RadioButton, IconButton, TouchableRipple } from 'react-native-paper';

// Firebase
import firebase from 'react-native-firebase';

// Navigation
import { translate } from '../../functions/translate';
import MGToolbarBack from '../../components/toolbar-back';

// Image Picker
import ImagePicker from 'react-native-image-picker';
// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
    title: translate('actions.selectAvatar'),
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
};

// Redux
import { connect } from 'react-redux';
import { updateUser } from '../../../redux/actions/user';
import AsyncStorage from '@react-native-community/async-storage';

class EditBasicScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            userLoaded: false,
            userUpdated: false,
            user: {
                fullName: '  ',
                age: '',
                gender: 'male',
            }
        };
    }

    componentWillUnmount(){
        this.setState({responseURI:null});
        this.getUserUnsubscribe();
    }

    getUser = async ()=>{
        const uid = await AsyncStorage.getItem('uid');

        if(!uid){return}
        this.getUserUnsubscribe = firebase.firestore()
            .collection('user').doc(uid)
            .onSnapshot(res=>{
                if (res.exists) {
                    const data = res.data();
                    this.setState({
                        user: data,
                        loading: false,
                        userUpdated: this.state.userLoaded
                    });

                    this.props.updateUser({
                        avatarImg: data.avatarImg,
                        fullName: data.fullName,
                        age: data.age,
                        gender: data.gender,
                    });
                }
            });
    }

    componentDidMount(){
        this.getUser();
    }

    save = async ()=>{
        try{
            this.setState({loading: true, userLoaded: true, userUpdated: false,});
            await firebase.firestore().collection('user').doc(this.props.user.uid).update(this.state.user);
            if(this.state.responseURI){
                await firebase.storage()
                    .ref(`avatars/${this.props.user.uid +'_'+ Math.ceil(Math.random()*1000)}`)
                    .putFile(this.state.responseURI)
            }else{
                this.setState({loading: false});
            }
        } catch(e){
            alert(e);
        }
    }

    selectAvatar = ()=>{
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {} 
            else if (response.error) {
                alert(response.error);
            } else {
                this.setState({responseURI:response.uri, loading: true});
                firebase.storage()
                    .ref(`avatars/${this.props.user.uid +'_'+ Math.ceil(Math.random()*1000)}`)
                    .putFile(this.state.responseURI)
                    .then()
                    .catch(err=>alert(err));
            }
        });
    }

    render() {
        return (
            <View style={{flex:1}}>
                <MGToolbarBack title={translate('profileEditScreen.title')} navigation={this.props.navigation} />
                <ScrollView 
                refreshControl={
                    <RefreshControl
                      refreshing={this.state.loading}
                    />
                }
                style={{backgroundColor: Colors.white}}>
                    <View>
                        <View style={{padding: 16, alignItems: 'center'}}>
                            <TouchableRipple disabled={this.state.loading} onPress={()=>this.selectAvatar()}>
                                {
                                    this.state.user.avatarImg?
                                    <Avatar.Image 
                                    key={this.state.user.avatarImg}
                                    source={{uri: this.state.user.avatarImg, cache: 'reload'}} 
                                    size={120} />
                                    :
                                    <Avatar.Text label={`${this.state.user.fullName[0].toUpperCase()}${this.state.user.fullName[1].toUpperCase()}`} size={120} />
                                }
                            </TouchableRipple>
                            <Button disabled={!this.state.user.avatarImg || this.state.loading} uppercase={false} style={{marginTop: 8, alignSelf: 'center'}} color={Colors.red500} mode="outlined" 
                            onPress={() => this.setState({ user:{...this.state.user, avatarImg: '' }})}>
                                {translate('actions.deleteImage')}
                            </Button>
                        </View>
                        <Divider />
                    </View>
                    <View style={{padding: 16}}>
                        <TextInput
                            disabled={this.state.loading}
                            mode='outlined'
                            style={{marginBottom: 16,}}
                            label={translate('form.fullName')}
                            value={this.state.user.fullName}
                            onChangeText={text => this.setState({ user:{...this.state.user, fullName: text }})}
                        />
                        <TextInput
                            disabled={this.state.loading}
                            mode='outlined'
                            label={translate('form.age')}
                            keyboardType='numeric'
                            maxLength={2}
                            value={this.state.user.age}
                            onChangeText={text => this.setState({ user:{...this.state.user, age: text }})}
                        />
                    </View>
                    <Divider />
                    <View>
                        <List.Subheader>{translate('form.gender')}</List.Subheader>
                        <View style={{padding: 16, paddingTop: 0,}}>
                            <TouchableRipple disabled={this.state.loading} onPress={() => { this.setState({ user:{...this.state.user, gender: 'male'}}); }}>
                                <View style={{alignItems: 'center', flexDirection: 'row'}}>
                                    <RadioButton
                                        disabled={this.state.loading}
                                        value="male"
                                        status={this.state.user.gender === 'male' ? 'checked' : 'unchecked'}
                                        onPress={() => { this.setState({ user:{...this.state.user, gender: 'male'}}); }}
                                    />
                                    <Text style={{marginLeft: 10}}>{translate('form.male')}</Text>
                                </View>
                            </TouchableRipple>
                            <TouchableRipple disabled={this.state.loading} onPress={() => { this.setState({ user:{...this.state.user, gender: 'female'}}); }}>
                                <View style={{alignItems: 'center', flexDirection: 'row'}}>
                                    <RadioButton
                                        disabled={this.state.loading}
                                        value="female"
                                        status={this.state.user.gender === 'female' ? 'checked' : 'unchecked'}
                                        onPress={() => { this.setState({ user:{...this.state.user, gender: 'female'}}); }}
                                    />
                                    <Text style={{marginLeft: 10}}>{translate('form.female')}</Text>
                                </View>
                            </TouchableRipple>
                        </View>
                    </View>
                </ScrollView>
                <View style={{padding: 16}}>
                    <Button disabled={this.state.loading} mode="contained" onPress={() => this.save()}>
                        {translate('actions.update')}
                    </Button>
                </View>
                <Snackbar
                    duration={1000}
                    visible={this.state.userUpdated}
                    onDismiss={() => this.setState({userUpdated: false})}
                >{translate('alert.userUpdated')}</Snackbar>
            </View>
        );
    }
}


const mapDispatchToProps = dispatch => {
    return {
        updateUser: (user)=>dispatch(updateUser(user)),
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditBasicScreen);
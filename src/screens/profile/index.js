import React, { Component } from 'react';
import { View } from 'react-native';
import { Colors, List, Divider, Surface } from 'react-native-paper';

import MGToolbarBack from '../../components/toolbar-back';
import { translate } from '../../functions/translate';
import { StackActions } from 'react-navigation';

export default class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            settings: [{
                icon: 'account-circle',
                id: "basic_info",
                routeName: "EditBasic",
            },{
                icon: 'map',
                id: "locations",
                routeName: "LocationSelect",
            },{
                icon: 'public',
                id: "your_nations",
                routeName: "NationSelect",
            }
        ]};
    }

    changeRoute = (routeName)=>{
        const pushAction = StackActions.push({
            routeName
        });
        
        this.props.navigation.dispatch(pushAction);
    }

    componentDidMount(){
        // this.changeRoute('LocationSelect');
    }

    renderList = (setting, i)=>(
        <View key={setting.id}>
            <List.Item
                left={(props)=><List.Icon {...props} icon={setting.icon} />}
                onPress={()=>this.changeRoute(setting.routeName)}
                right={()=><List.Icon color={Colors.grey800} icon={'chevron-right'} />}
                title={translate(`profileScreen.settings.${setting.id}`)}
            />
            {
                this.state.settings.length!=(i+1)?
                <Divider />:null
            }
        </View>
    )

    render() {
        return (
            <View style={{flex:1, backgroundColor: Colors.grey300,}}>
                <MGToolbarBack title={translate('profileScreen.title')} navigation={this.props.navigation} />

                <Surface style={{elevation: 1}}>
                    
                </Surface>

                <List.Section style={{backgroundColor: Colors.white, marginBottom: 0}}>
                    {this.state.settings.map((setting, i)=>this.renderList(setting, i))}
                </List.Section>
            </View>
        );
    }
}
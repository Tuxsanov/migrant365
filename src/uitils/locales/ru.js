export default {
    worldWide: 'По всему миру',
    drawer: {
        unreadPosts: 'обновления'
    },
    loginScreen: {
        title: "Login",
    },
    registerScreen: {
        title: "Register",
    },
    homeTab: {
        // places: "Places",
        forums: "Форум",
        events: "События",
        jobs: "Работа",
        arrivals_departures: "Прибытие и отъезд",
    },
    homeTabPlaces: {
        info: "Places recomended by locals and travellers. Feel free to share you recomendation with users",
        list: {
            hotel_hostels: "Hotels & Hostels",
            home_appartments: "Home & Appartments",
            eat_out: "Eat Out",
            malls: "Malls",
            medical_centers: "Medical centers",
            vehicle_rental: "Vehicle rental",
            travel_agencies: "Travel agencies",
        }
    },
    addPost: {
        title: 'Create',
    },
    selectCity: {
        title: 'Выберите город',
    },
    recomendPlaceScreen: {
        title: "Recomend a Place",
        terms: "Avoid bad words it leads you to not recomend next time!"
    },
    profileScreen: {
        title: "Профиль",
        settings: {
            basic_info: "О пользователе",
            locations: "Расположение пользователя",
            your_nations: "Национальность пользователя",
        }
    },
    profileEditScreen: {
        title: "Редактировать профиль",
    },
    locationNationScreen: {
        title: "Locations & Nations",
    },
    locationSelectScreen: {
        title: "Выберите страны"
    },
    OriginSelectScreen: {
        title: "Select City"
    },
    actions: {
        close: "Закрыть",
        submit: "Submit",
        recommendPlace: "Recommend Place",
        pleaseSelectCategory: "Please Select Category",
        selectAvatar: "Select Avatar",
        logout: "Logout",
        signIn: "Sign In",
        forgotPassword: "Forgot the password?",
        signInWithFaceBook: "Sign in with Facebook",
        signInWithGoogle: "Sign in with Google",
        createAnAccount: "Create an account",

        update: 'Сохранить',
        deleteImage: "Удалить изображение",
        selectCity: "Город & Национальность",
        selectNationality: "Национальность",
        selectCategory: "Категория"
    },
    form: {
        phoneNumber: 'Phone Number',
        fullName: 'ФИО',
        age: 'Возраст',
        email: 'Эл. адрес',
        password: 'Password',
        gender: 'Пол',
        male: 'Мужчина',
        female: 'Женщина',
        updated: 'Updated',
        filter: 'Фильтр',
        allCategory: 'Все категории',
        allNationality: 'Все национальности',
    },
    formError: {
        emailOrPassIncorrect: 'Email or password did not matched',
        emailAlreadyinUse: 'The email already in use',
        emailIncorrect: 'Email address is invalid!',
        passwordLengthShort: 'Password length should be at least 8 characters'
    },
    alert: {
        userUpdated: "Пользователь обновлен"
    }
};
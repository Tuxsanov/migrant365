export default {
    loginScreen: {
        title: "Login",
    },
    registerScreen: {
        title: "Register",
    },
    homeTab: {
        places: "Places",
        forums: "Forums",
        events: "Events",
        jobs: "Jobs",
        arrivals_departures: "Arrivals and departurs",
    },
    homeTabPlaces: {
        info: "Places recomended by locals and travellers. Feel free to share you recomendation with users",
        list: {
            hotel_hostels: "Hotels & Hostels",
            home_appartments: "Home & Appartments",
            eat_out: "Eat Out",
            malls: "Malls",
            medical_centers: "Medical centers",
            vehicle_rental: "Vehicle rental",
            travel_agencies: "Travel agencies",
        }
    },
    addPost: {
        title: 'Create',
    },
    recomendPlaceScreen: {
        title: "Recomend a Place",
        terms: "Avoid bad words it leads you to not recomend next time!"
    },
    profileScreen: {
        title: "Profile",
        settings: {
            basic_info: "Basic info",
            locations: "Locations",
            your_nations: "Nationality",
            verifications: "Verify identity",
            notifications: "Notifications",
            points: "Points",
            company_shares: "Your Company Shares",
        }
    },
    profileEditScreen: {
        title: "Edit Basics",
    },
    locationNationScreen: {
        title: "Locations & Nations",
    },
    locationSelectScreen: {
        title: "Select Countries"
    },
    OriginSelectScreen: {
        title: "Select City"
    },
    actions: {
        submit: "Submit",
        recommendPlace: "Recommend Place",
        pleaseSelectCategory: "Please Select Category",
        selectAvatar: "Select Avatar",
        logout: "Logout",
        signIn: "Sign In",
        forgotPassword: "Forgot the password?",
        signInWithFaceBook: "Sign in with Facebook",
        signInWithGoogle: "Sign in with Google",
        createAnAccount: "Create an account",
    },
    form: {
        phoneNumber: 'Phone Number',
        fullName: 'Full Name',
        name: 'Name',
        lastName: 'Last Name',
        age: 'Age',
        email: 'Email',
        password: 'Password',
        update: 'Update',
        selectGender: 'Select Gender',
        male: 'Male',
        female: 'Female',
        updated: 'Updated',
    },
    formError: {
        emailOrPassIncorrect: 'Email or password did not matched',
        emailAlreadyinUse: 'The email already in use',
        emailIncorrect: 'Email address is invalid!',
        passwordLengthShort: 'Password length should be at least 8 characters'
    }
};
import React, { Component } from 'react';
import { StatusBar, View, Image, TouchableOpacity, Text } from 'react-native';
import { Appbar, Button, Surface, Colors, Title, Avatar, Divider, Badge, IconButton, Subheading, Caption, TouchableRipple, } from 'react-native-paper';
import { translate } from '../functions/translate';

import Emoji from 'react-native-emoji';
import { robotoWeights, material } from 'react-native-typography';
import { StackActions } from 'react-navigation';

export default class MGToolbar extends Component {
    constructor(props){
        super(props);
        this.state = {
            selectedCountry: {}
        };
    }

    changeRoute = (routeName)=>{
        const pushAction = StackActions.push({
            routeName
        });
        
        this.props.navigation.dispatch(pushAction);
    }

    render() {
        const {navigation, user} = this.props;
        const {selectedCountryName = ''} = user;
        return (
            <View>
                <StatusBar backgroundColor={Colors.black} barStyle="light-content" />
                <Surface style={{minHeight: 56, elevation: 4, zIndex: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <Image 
                        style={{marginRight: 'auto', marginLeft: 12, height: 174/8, width: 826/8}}
                        source={require('../../imgs/logo.png')} 
                    />
                    {/* <Button uppercase={false} style={{marginLeft: 'auto'}} mode="text" onPress={() => console.log('Pressed')}>
                        Ask question
                    </Button> */}
                    <IconButton
                        icon="create"
                        color={Colors.red600}
                        size={26}
                        onPress={()=>this.changeRoute('PostAdd')}
                    />
                    <IconButton
                        icon="sort"
                        color={Colors.black}
                        size={26}
                        onPress={()=>navigation.openDrawer()}
                    />
                    {/* <IconButton
                        style={{marginLeft: 'auto'}}
                        icon="create"
                        color={Colors.black}
                        size={26}
                        onPress={() => console.log('Pressed')}
                    /> */}
                </Surface>
                <Divider />
            </View>
        );
    }
}

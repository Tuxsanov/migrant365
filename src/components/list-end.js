import * as React from 'react';
import { View, Text } from 'react-native';
import { material } from 'react-native-typography';
export default class ListEndComponent extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <View style={{padding: 10, paddingTop:50, alignItems: 'center'}}>
                <Text style={[material.body1]}>
                    End list
                </Text>
                <Text style={[material.caption]}>
                    migrant365.com
                </Text>
            </View>
        )
    }
}
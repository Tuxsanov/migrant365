import React, { Component } from 'react';
import { View } from 'react-native';
import { Avatar, Colors } from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class MGAvatar extends Component {
    render() {
        const {user, size} = this.props; 
        const {avatarImg, name, lastname} = user;
        return (
            <View style={{
                justifyContent: 'center',
                alignItems: 'flex-end',
            }}>
                {
                    avatarImg?
                    <Avatar.Image size={size} source={{uri: avatarImg}} />
                    :
                    <Avatar.Text size={size} label={`${name?`${name.charAt(0)}${lastname.charAt(0)}`:''}`} />
                }
                {/* <MaterialIcons color={Colors.green500} name="lens" size={size/3} style={{top: -(size/3)}} />                 */}
            </View>
        )
    }
}

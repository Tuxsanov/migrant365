import React, { Component } from 'react';
import { View, StatusBar, } from 'react-native';
import { Appbar, Colors, Surface } from 'react-native-paper';
import { NavigationActions } from 'react-navigation';

export default class MGToolbarBack extends Component {
    state = {
        visible: false,
    };
    
    render() {
        const {navigation, title} = this.props;
        return (
            <View>
                <Surface style={{zIndex: 99, elevation: 4}}>
                    <Appbar.Header>
                        <Appbar.BackAction onPress={()=>(
                            navigation.dispatch(NavigationActions.back())
                        )} />
                        <Appbar.Content
                            title={title}
                        />
                    </Appbar.Header>
                </Surface>
            </View>
        );
    }
}
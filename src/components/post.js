import React from "react";
import {
  View,
  Dimensions,
  TouchableHighlight,
} from "react-native";
import { material } from 'react-native-typography';
import { Avatar, Button, Card, Title, Paragraph, IconButton, Divider } from 'react-native-paper';

import { withPreventDoubleClick } from "./preventDoubleClick";


export default class MGPost extends React.Component {
    constructor(props){
        super(props);

        this.state = {}
    }
    
    render() {
        return (
            <Card style={[{marginTop: 10, }]}>
                <Card.Title
                    title={`Reyna Chung`}
                    subtitle="24 minutes ago"
                    left={(props) => <Avatar.Image {...props} source={{uri: 'https://tinyfac.es/data/avatars/26CFEFB3-21C8-49FC-8C19-8E6A62B6D2E0-200w.jpeg'}} />}
                    right={(props) => <IconButton
                        icon="more-horiz"
                        style={{marginLeft: 'auto'}}
                        onPress={() => console.log('Pressed')}
                    />}
                />
                <Card.Content>
                    <Title>Погрузчик</Title>
                    <Title style={material.subheading}>17 000 rubles / month</Title>
                    <Paragraph numberOfLines={2}>
                        На завод морской рыбы требуются люди
                        Рабочий день с 8:00 до 18:00 (В воскресенье выходной)
                        Зарплата каждый день по 105000 with other
                    </Paragraph>
                </Card.Content>
                {/* <Card.Cover source={{ uri: 'https://images.unsplash.com/photo-1500822976077-ea303cfdc9b4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60' }} /> */}
                <Card.Content>
                    <View style={{flexDirection: 'row', paddingVertical: 10}}>
                        <Paragraph style={[material.caption, {marginRight: 10,}]}>12 Applied</Paragraph>
                        <Paragraph style={[material.caption, {marginLeft: 'auto'}]}>32 Interested</Paragraph>
                    </View>
                    <Divider />
                </Card.Content>
                <Card.Actions style={{justifyContent: 'space-between'}}>
                    <Button icon={'attach-file'} mode="text" onPress={() => alert('Apply!')}>
                        Apply
                    </Button>
                    <Button icon="favorite-border" mode={'text'} onPress={() => console.log('Pressed')}>
                        Interested
                    </Button>
                </Card.Actions>
            </Card>
        )
    }
}

import React from 'react'
import { 
    StyleSheet,
    View,
    Text
} from "react-native";
import { material } from 'react-native-typography';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { FAB } from 'react-native-paper';

export default class MGPostFilter extends React.Component {
  render() {
    return (
        <FAB
            style={styles.fab}
            small
            icon="add"
            onPress={() => console.log('Pressed')}
        />
        // <View style={styles.wrapper}>
        //     <Text style={material.button}>Tadbirlar / 2 Manzil</Text>
        //     <View style={styles.filter}>
        //         <MaterialIcons color="#fff" size={22} name="filter-list" />
        //     </View>
        // </View>
    )
  }
}

const styles = StyleSheet.create({
    wrapper: {
        paddingHorizontal:15,
        paddingVertical: 10,
        alignItems: 'center',
        flexDirection:'row',
    },
    filter: {
        marginLeft: 'auto',
        width:40,
        height:40,
        padding: 0,
        borderRadius: 40,
        backgroundColor: "#000",
        justifyContent:'center',
        alignItems: 'center',
    },
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
    },
});
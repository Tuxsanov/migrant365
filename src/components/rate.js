import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { material, iOSColors, robotoWeights } from 'react-native-typography';
import UserWrap from './userWrap';


const RateIcon = ({value})=>{
    const rateLength = [0,1,2,3,4];
    return (
        rateLength.map(e=>
            <MaterialIcons 
                key={e} 
                style={{marginLeft: 1}} 
                color={value<=5?iOSColors.yellow:iOSColors.lightGray} 
                size={12} 
                name={(Math.floor(value)>=(e+1))?"star":((Math.round(value)==(e+1))?"star-half":"star-border")} 
            />
        )
    )
}

const Rate = ({value})=>{
    return(
        <View style={{flexDirection: 'row',}}>
            <RateIcon value={value} />
            <Text style={{...material.caption,marginLeft: 10}}>
                {value}/5
            </Text>
        </View>
    )
}

export default class MGRate extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const {title} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.box}>
                    <Text style={[material.subheading, robotoWeights.bold]} numberOfLines={1}>
                        {title}
                    </Text>

                    <Rate value={4.6} />
                    
                    <Text style={material.body1}>
                        Everything was amazing and solid will recommend everyone
                    </Text>
                </View>
                <View style={styles.userWrap}>
                    <UserWrap />
                    <Text style={material.caption}>12 Dec 2018</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15,
        elevation: 1,
    },
    box: {
        backgroundColor: '#fff',
        borderRadius: 4,
        padding: 15,
    },
    userWrap: {
        flexDirection: 'row',
        paddingVertical: 8,
        paddingHorizontal: 15,
        backgroundColor: '#fafafa',
    }
})
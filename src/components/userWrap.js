import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Avatar } from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { material, iOSColors } from 'react-native-typography';


export default class UserWrap extends Component {
    getNameLabel(name){
        let labelName = '';
        let nameArr = name.split(' ');
        if(nameArr.length==1){
            nameArr[1] = '_';
        }
        labelName += nameArr[0].charAt(0);
        labelName += nameArr[1].charAt(0);
        return labelName.toUpperCase();
    }
    render() {
        const {name = 'Rebeth Duer'} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.avatarWrap}>
                    <Avatar.Text size={36} label={this.getNameLabel(name)} />
                    <View style={styles.onlineIndicator}></View>
                </View>

                <View style={{justifyContent: 'center'}}>
                    <View style={styles.nameWrap}>
                        <Text style={[material.body2, {lineHeight: 18}]}>{name}</Text>
                        <MaterialIcons style={{marginLeft: 5}} color="orange" size={14} name="check-box" />
                    </View>
                    <Text style={material.caption}>Uzbekistan</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1, dont use
        flexDirection: 'row',
        alignItems: 'center',
    },
    avatarWrap: {
        flexDirection: 'row',
        marginRight: 10,
    },
    onlineIndicator: {
        width: 10,
        height: 10,
        borderRadius: 10,
        alignSelf: 'flex-end',
        marginLeft: -10,
        backgroundColor: iOSColors.green,
    },
    nameWrap: {
        flexDirection: 'row',
        alignItems: 'center',
    }
})
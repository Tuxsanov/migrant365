import React, {Component} from 'react';
import { RefreshControl, View, StyleSheet, ScrollView, Text } from 'react-native';

import { DrawerItems, SafeAreaView } from 'react-navigation';
import { Avatar, Button, Divider, Chip, List, Colors, Card, Title, Caption, Surface, Subheading, RadioButton, TouchableRipple, IconButton, } from 'react-native-paper';
import { material, iOSColors, robotoWeights } from 'react-native-typography';
import { homeStyles } from '../screens/home/styles';
import { changeRoute } from '../functions/route';
// Lodash
import { orderBy, findIndex, cloneDeep, filter } from 'lodash';
// Redux
import { connect } from 'react-redux';
import MGAvatar from './avatar';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { addUser, updateUser } from '../../redux/actions/user';
import { translate } from '../functions/translate';

class CustomDrawerContentComponent extends Component{
    
    constructor(props){
        super(props);

        this.state = {
            loading: true
        }
    }

    loadUser = async ()=>{
        const uid = await AsyncStorage.getItem('uid');

        if(!uid){return}

        const user = await firebase.firestore().collection('user').doc(uid).get();
        
        if (user.exists) {
            let _user = user.data();
            _user.uid = uid;
            this.props.addUser(_user);
        }

        this.setState({loading: false});

        // this.changeRoute('PostAdd');
    }

    componentDidMount(){
        this.loadUser();
    }

    changeRoute = (routeName)=>{
        this.props.navigation.closeDrawer();
        changeRoute(this.props.navigation, routeName, 'push');
    }

    render(){
        return(
            <ScrollView 
            refreshControl={
                <RefreshControl
                  refreshing={this.state.loading}
                  onRefresh={()=>this.loadUser()}
                />
            }
            contentContainerStyle={[homeStyles.flatList, {zIndex: 99999}]}>
                <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
                    <List.Item
                        title="Category"
                        // description="Item description"
                        right={props => <List.Icon {...props} icon="chevron-right" />}
                    />
                </SafeAreaView>
            </ScrollView>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addUser: (user)=>dispatch(addUser(user)),
        updateUser: (user)=>dispatch(updateUser(user)),
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomDrawerContentComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.grey300,
  },
});
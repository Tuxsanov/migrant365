import I18n from "../uitils/I18n";

export const translate = (str)=>{
    return I18n.t(str, {locale: "ru"})
}
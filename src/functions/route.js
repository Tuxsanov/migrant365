import { StackActions } from "react-navigation";

export const changeRoute = (navigation, routeName, action = 'push', params)=>{
    const pushAction = StackActions[action]({
        routeName,
        params,
    });
    
    navigation.dispatch(pushAction);
}
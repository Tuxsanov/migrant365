import firebase from 'react-native-firebase';
const auth = firebase.auth();

import AsyncStorage from "@react-native-community/async-storage";

// export const getUID = await AsyncStorage.getItem('uuid');

saveCredential = ({user})=>{
    AsyncStorage.setItem('uid', user.uid);
}

export const signInAnonymously = ()=>{
    auth.signInAnonymously()
        .then(credential => {
            saveCredential(credential)
        });
}
import React from "react";
import { View, createBottomTabNavigator, createDrawerNavigator } from "react-navigation";
import { Text, Colors, Badge, Avatar, } from "react-native-paper";
import { material, robotoWeights } from "react-native-typography";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ForumScreen from "../screens/forum";
import HomeScreen from "../screens/home";
import CustomDrawerContentComponent from "../components/drawer";
import ProfileScreen from "../screens/profile";

// Redux
import { connect } from 'react-redux';
import { TabNavBottomAvatar } from "./tabNavBottomAvatar";

const TabNavigatorBottom = createBottomTabNavigator({
    Forum: HomeScreen,
    Event: HomeScreen,
    Job: HomeScreen,
    Flight: HomeScreen,
    Profile: ProfileScreen,
}, {
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarLabel: ()=>{return null},
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            let IconComponent = MaterialIcons;
            let iconName;
            
            if (routeName === 'Forum') {
                iconName = 'map';
                // IconComponent = NavIconWithBadge; 
            }

            if (routeName === 'Event') {
                iconName = 'inbox';
            }

            if (routeName === 'Job') {
                iconName = 'notifications';
            }

            if (routeName === 'Flight') {
                iconName = 'show-chart';
            }

            if(routeName === 'Profile') {
                return <TabNavBottomAvatar />
            }else{
                return <IconComponent name={iconName} style={{marginTop: 4}} size={24} color={tintColor} />;
            }
    
        },
    }),
    tabBarOptions: {
        activeTintColor: '#007dc6',
        inactiveTintColor: Colors.grey500,
        style: {
            borderTopColor: Colors.grey300,
        }
    },
});

export const DrawerTabNavigatorBottom = createDrawerNavigator({
    HomeTabForum: {
        screen: TabNavigatorBottom,
    }
}, {
    drawerPosition: 'right',
    contentComponent: CustomDrawerContentComponent
});

DrawerTabNavigatorBottom.navigationOptions = {
    header: null,
}

TabNavigatorBottom.navigationOptions = {
    header: null,
}
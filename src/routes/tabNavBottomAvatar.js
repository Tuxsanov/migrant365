import React from "react";
import { Avatar } from "react-native-paper";

// Redux
import { connect } from 'react-redux';

const TabAvatar = (props) => {
    return (
        props.user?
        <Avatar.Image 
            size={32} 
            style={{alignSelf: 'center',}} 
            source={{uri: props.user.avatarImg}} 
        />:
        <Avatar.Text
            label={props.user.fullName.slice(0,2)}
            size={32} 
            style={{alignSelf: 'center',}} 
        />
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};

export const TabNavBottomAvatar = connect(mapStateToProps)(TabAvatar);
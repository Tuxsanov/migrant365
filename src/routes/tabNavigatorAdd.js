import React from "react";
import { View } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import { iOSColors } from "react-native-typography";

export default class TabNavigatorAddBtn extends React.Component {
  render(){
    return (
      <View style={{ 
        borderRadius: 50,
        backgroundColor: iOSColors.black,
        justifyContent: 'center',
        alignItems: 'center',
        width: 44, height: 44, 
      }}>
        <MaterialIcons color={iOSColors.red} size={32} name="add" />
      </View>
    )
  }
}

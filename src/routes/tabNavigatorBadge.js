import React from "react";
import { 
  View,
  Text
} from "react-native";

// import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { iOSColors } from "react-native-typography";

export default class IconWithBadge extends React.Component {
  render() {
    const { name, badgeCount, color, size } = this.props;
    return (
      <View style={{ width: 24, height: 24, margin: 5 }}>
        <FontAwesome5 style={{marginTop: 4}} name={name} size={size} color={color} />
        { badgeCount > 0 && (
          <View style={{
            // If you're using react-native < 0.57 overflow outside of the parent
            // will not work on Android, see https://git.io/fhLJ8
            position: 'absolute',
            right: -3,
            top: 2,
            backgroundColor: iOSColors.red,
            borderRadius: 10,
            minWidth: 10,
            minHeight: 10,
            justifyContent: 'center',
            alignItems: 'center',
            opacity: .5,
          }}>
            {/* <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>{badgeCount}</Text> */}
          </View>
        )}
      </View>
    );
  }
}
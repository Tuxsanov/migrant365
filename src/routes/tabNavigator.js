import React from "react";
import {StyleSheet} from "react-native";
import { createBottomTabNavigator, createAppContainer, createStackNavigator, createDrawerNavigator, SafeAreaView, DrawerItems } from "react-navigation";
// import CardStackStyleInterpolator from 'react-navigation/lib/views/CardStack/CardStackStyleInterpolator';
// react-navigation-stack/src/views/StackView/StackViewStyleInterpolator

import CardStackStyleInterpolator from 'react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator';
// import StackViewStyleInterpolator from 'react-navigation-stack/src/views/StackView/StackViewStyleInterpolator'

import HomeScreen from "../screens/home";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import TabNavigatorAddBtn from "./tabNavigatorAdd";
import IconWithBadge from "./tabNavigatorBadge";
import AddPostScreen from "../screens/add-post";
import AddPostFormScreen from "../screens/post-add";
import SettingsScreen from "../screens/settings";
import ChatListScreen from "../screens/chat-list";
import NotificationsScreen from "../screens/notifications";
import LoginScreen from "../screens/auth/login";
import { Text } from "react-native-paper";
import { material, robotoWeights, iOSColors } from "react-native-typography";
import { ScrollView } from "react-native-gesture-handler";
import OrderHomePosts from "../screens/home/order";
import PostDetailScreen from "../screens/post-detail";
import CustomDrawerContentComponent from "../components/drawer";
import EventDetailsComponent from "../screens/event-details";
import ForumCategoriesPage from "../screens/home/filters/forum-categories";
import ForumFilterPage from "../screens/home/filters/forum-filter";
import SelectCityScreen from "../screens/home/filters/select-city";
import CountryListScreen from "../screens/country-list";
import RecommendPlacesPage from "../screens/recommendPlace";
import ProfileScreen from "../screens/profile";
import EditBasicScreen from "../screens/profile/edit-basic";
import LocationSelectScreen from "../screens/location-nation/location";
import NationSelectScreen from "../screens/location-nation/nation";
import OriginSelectScreen from "../screens/location-nation/origin";
import RegisterScreen from "../screens/auth/register";
import ChatScreen from "../screens/chat";
import { DrawerTabNavigatorBottom } from "./tabNavBottom";
import ImageAlbumsScreen from "../screens/image/album";
import ImageGalleryScreen from "../screens/image/gallery";
// Redxux
// import { connect } from 'react-redux';

const NavIconWithBadge = (props) => {
    // You should pass down the badgeCount in some other ways like react context api, redux, mobx or event emitters.
    return <IconWithBadge {...props} badgeCount={+1} />;  
}

const TabNavigator = createBottomTabNavigator({
    Profile: {
        screen: ProfileScreen,
        navigationOptions: {
            header: null
        }
    },
    Notifications: NotificationsScreen,
    // Add: {
    //     screen: AddPostScreen,
    //     navigationOptions: {
    //     tabBarLabel: ()=>{}
    //     }
    // },
    Messages: ChatListScreen,
    Me: SettingsScreen,
}, {
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarLabel: ({ focused, horizontal, tintColor })=>{
            const { routeName } = navigation.state;      
            return <Text style={[material.caption,robotoWeights.medium,{
                textAlign: 'center',
                marginBottom: 4,
                color: tintColor,
            }]}>{routeName}</Text>;
        },
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            let IconComponent = MaterialIcons;
            let iconName;
            
            if (routeName === 'Profile') {
                iconName = 'settings';
                // IconComponent = NavIconWithBadge; 
            } else if (routeName === 'Notifications') {
                iconName = 'settings';
            }
            //  else if (routeName === 'Add') {
            //     IconComponent = TabNavigatorAddBtn;
            // } 
            else if (routeName === 'Messages') {
                iconName = 'settings';
            } else if (routeName === 'Me') {
                iconName = 'label';
            }
    
            // You can return any component that you like here!
            return <IconComponent name={iconName} style={{marginTop: 4}} size={20} color={tintColor} />;
        },
    }),
    tabBarOptions: {
        activeTintColor: '#000',
        inactiveTintColor: iOSColors.lightGray2,
        style: {
            borderTopColor: 'rgba(0,0,0,0.05)',
        }
    }
});

TabNavigator.navigationOptions = {
    header: null,
}


// const MyDrawerNavigator = createDrawerNavigator({
//     // Home: {
//     //   screen: AppNavigator,
//     // }
//     // AppNavigator
// }, {
//     drawerPosition: 'left',
//     contentComponent: CustomDrawerContentComponent
//     // contentComponent: ()=><CustomDrawerContentComponent />, 
// });

// MyDrawerNavigator.navigationOptions = {
//     header: null,
// }

const customNavStyle = {
    headerStyle: {
        elevation: 0,
        backgroundColor: iOSColors.black,
    },
    headerTintColor: iOSColors.white,
}


// -------------
// class Count extends React.Component {
//     render() {
//       return <Text>Count: {this.props.value}</Text>
//     }
// }
  
// let CountContainer = connect(state => ({ value: 'state' }))(Count);
// -------------

const AppNavigator = createStackNavigator({
    DrawerTabNavigatorBottom,
    LoginScreen: {
        screen: LoginScreen,
        navigationOptions: {
            header: null,
            // title: 'Auth'
        }
    },
    ImageGallery: {
        screen: ImageGalleryScreen,
        navigationOptions: {header: null}
    },
    ImageAlbums: {
        screen: ImageAlbumsScreen,
        navigationOptions: {header: null}
    },
    SelectCity: {
        screen: SelectCityScreen,
        navigationOptions: {
            header: null,
            // title: 'Auth'
        }
    },
    PostAdd: {
        screen: AddPostFormScreen,
        navigationOptions: {
            header: null,
        }
    },
    // Home: MyDrawerNavigator,
    TabNavigator,
    // Profile: {
    //     screen: ProfileScreen,
    //     navigationOptions: {
    //         header: null
    //     }
    // },
    LocationSelect: {
        screen: LocationSelectScreen,
        navigationOptions: {
            header: null,
            // title: <CountContainer />
            // transitionConfig: () => ({
            //     screenInterpolator: sceneProps => {
            //         return CardStackStyleInterpolator.forHorizontal(sceneProps);
            //     }
            // }),
        }
    },
    EditBasic: {
        screen: EditBasicScreen,
        navigationOptions: {
            header: null
        }
    },
    NationSelect: {
        screen: NationSelectScreen,
        navigationOptions: {
            header: null
        }
    },
    CountryList: {
        screen: CountryListScreen,
        navigationOptions: {
            header: null
        }
    },
    Register: {
        screen: RegisterScreen,
        navigationOptions: {
            header: null,
        }
    },
    PostDetails: {
        screen: PostDetailScreen,
        navigationOptions: {
            header: null,
            // title: 'Details',
            // ...customNavStyle,
        }
    },
    ChatScreen: {
        screen: ChatScreen,
        navigationOptions: {
            header: null,
            // title: 'Chat'
        }
    },
    OriginSelect: {
        screen: OriginSelectScreen,
        navigationOptions: {
            header: null
        }
    },
    RecommendPlaces: {
        screen: RecommendPlacesPage,
        navigationOptions: {
            header: null
        }
    },
    ForumFilter: {
        screen: ForumFilterPage,
        navigationOptions: {
            header: null
        }
    },
    ForumCategories: {
        screen: ForumCategoriesPage,
        navigationOptions: {
            header: null
        }
    },
    EventDetails: {
        screen: EventDetailsComponent,
        navigationOptions: {
            title: 'Event Details',
            ...customNavStyle,
        }
    },
    OrderHomePosts: {
        screen: OrderHomePosts,
        navigationOptions: {
            title: 'Filter',
            ...customNavStyle,
        }
    },
    Notifications: {
        screen: NotificationsScreen,
        navigationOptions: {
            title: 'Notifications',
            ...customNavStyle,
        }
    },
    // Add: {
    //     screen: AddPostScreen,
    //     navigationOptions: {
    //     tabBarLabel: ()=>{}
    //     }
    // },
    Messages: {
        screen: ChatListScreen,
        navigationOptions: {
            title: 'Messages',
            ...customNavStyle,
        }
    },
    Me: SettingsScreen,

    // Home: TabNavigator,
    SettingsScreen
}, {
    transitionConfig: () => {
        return {screenInterpolator: props => {
            // Transitioning to search screen (navigate)
            if (props.scene.route.routeName === 'LocationSelect') {
              return CardStackStyleInterpolator.forNoAnimation(props);
            }

            if (props.scene.route.routeName === 'PostAdd') {
              return CardStackStyleInterpolator.forFade(props);
            }

            if (props.scene.route.routeName === 'PostDetails') {
                return CardStackStyleInterpolator.forFadeFromBottomAndroid(props);
            }
      
            const last = props.scenes[props.scenes.length - 1];
      
            // Transitioning from search screen (goBack)
            if (last.route.routeName === 'LocationSelect') {
              return CardStackStyleInterpolator.forNoAnimation(props);
            }

            if (last.route.routeName === 'PostAdd') {
                return CardStackStyleInterpolator.forFade(props);
            }

            if (last.route.routeName === 'PostDetails') {
                return CardStackStyleInterpolator.forFadeFromBottomAndroid(props);
            }
      
            return CardStackStyleInterpolator.forHorizontal(props);
        }}
        // const isBack = fromTransitionProps.navigation.state.index >= toTransitionProps.navigation.state.index;
        // const routeName = isBack ? fromTransitionProps.scene.route.routeName : toTransitionProps.scene.route.routeName;

        // // This check is only for the case where the transitionConfig is specified globally per navigator basis
        // // If the config is specified per screen basis, then `routeName` will always refer to the current screen
        // if (routeName === 'LocationSelect') {
        //     return {
        //         // transitionSpec: { duration: isBack ? 150 : 200 },
        //         screenInterpolator: CardStackStyleInterpolator.forVertical(props),
        //     }
        // }else{
        //     return {
        //         screenInterpolator: CardStackStyleInterpolator.forHorizontal(props),
        //         // screenInterpolator: sceneProps => {
        //         //     return CardStackStyleInterpolator.forHorizontal(sceneProps);
        //         // }
        //     }

        // }
    },
    // transitionConfig: () => ({
    //     screenInterpolator: sceneProps => {
    //         return CardStackStyleInterpolator.forHorizontal(sceneProps);
    //     }
    // }),
}
);


// AppRegistry.registerComponent('RNNavigators', () => Drawer );
export default createAppContainer(AppNavigator);
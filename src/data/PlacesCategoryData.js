export const PlacesCategoryData = [{
    id: 'hotel_hostels',
    icon: 'hotel'
},{
    id: 'home_appartments',
    icon: 'home'
},{
    id: 'eat_out',
    icon: 'restaurant'
},{
    id: 'malls',
    icon: 'local-mall'
},{
    id: 'medical_centers',
    icon: 'local-hospital'
},{
    id: 'vehicle_rental',
    icon: 'directions-car'
},{
    id: 'travel_agencies',
    icon: 'map'
}]
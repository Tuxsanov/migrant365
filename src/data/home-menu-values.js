export const data = [
    {
        title: 'Hotels & Hostels',
        icon: 'hotel'
    },{
        title: 'Home & Apartments',
        icon: 'home'
    },{
        title: 'Eat out',
        icon: 'restaurant'
    },{
        title: 'Malls',
        icon: 'local-mall'
    },{
        title: 'Medical centers',
        icon: 'local-hospital'
    },{
        title: 'Vehicle rental',
        icon: 'directions-car'
    },{
        title: 'Travel agencies',
        icon: 'map'
    },
];
import AsyncStorage from "@react-native-community/async-storage";

export const storeData = async (storage_key, stored_value) => {
    try {
        await AsyncStorage.setItem(storage_key, stored_value)
    } catch (e) {
    }
}

export const getData = async (storage_key) => {
    try {
        const value = await AsyncStorage.getItem(storage_key)
        if(value !== null) {
            return value;
        }
    } catch(e) {
    }
}

export const removeData = async (storage_key) => {
    try {
        const value = await AsyncStorage.removeItem(storage_key)
    } catch(e) {
    }
}
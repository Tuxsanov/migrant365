import firebase from 'react-native-firebase'
import { storeData } from '../storage';
import { changeRoute } from '../functions/route';

export async function emailRegister(email, password, navigation) {
  try {
    const registerUserCredentials = await firebase.auth().createUserWithEmailAndPassword(email, password)

    if(registerUserCredentials.user){
      storeData('user', JSON.stringify(registerUserCredentials.user));
    }
    
    changeRoute(navigation, 'Profile');
  } catch (e) {
    alert(e);
  }
}
import firebase from 'react-native-firebase'
import { storeData } from '../storage';
import { changeRoute } from '../functions/route';

export async function emailLogin(email, password, navigation) {
  try {
    const loginUserCredentials = await firebase.auth().signInWithEmailAndPassword(email, password)
  
    if(loginUserCredentials.user){
      storeData('user', JSON.stringify(loginUserCredentials.user));
    }

    changeRoute(navigation, 'Profile');    
  } catch (e) {
    alert(e);
  }
}
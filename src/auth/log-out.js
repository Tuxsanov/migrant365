import firebase from 'react-native-firebase'
import { storeData, removeData } from '../storage';

export async function logOut() {
  try {
    firebase.auth().signOut();
    removeData('user');
  } catch (e) {
    alert(e);
  }
}
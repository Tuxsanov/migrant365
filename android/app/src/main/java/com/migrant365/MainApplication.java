package com.migrant365;

import android.app.Application;

// FBSDK
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.facebook.appevents.AppEventsLogger;
// 
import com.facebook.react.ReactApplication;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.imagepicker.ImagePickerPackage;
import com.reactcommunity.rnlocalize.RNLocalizePackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

// 
import com.rnfs.RNFSPackage;
// 
import com.wix.RNCameraKit.RNCameraKitPackage;

// FIREBASE PACKAGES
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.auth.RNFirebaseAuthPackage;
import io.invertase.firebase.firestore.RNFirebaseFirestorePackage;
import io.invertase.firebase.database.RNFirebaseDatabasePackage;
import io.invertase.firebase.storage.RNFirebaseStoragePackage;

import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

  protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
        new MainReactPackage(),
          new RNFSPackage(),
            new RNGoogleSigninPackage(),
        new FBSDKPackage(mCallbackManager),
            new ImagePickerPackage(),
            new RNLocalizePackage(),
            new AsyncStoragePackage(),
        new VectorIconsPackage(),
        new RNGestureHandlerPackage(),


        // 
        new RNCameraKitPackage(),
        // 

        new RNFirebasePackage(),
        new RNFirebaseAuthPackage(),
        new RNFirebaseFirestorePackage(),
        new RNFirebaseDatabasePackage(),
        new RNFirebaseStoragePackage(),

        new RNFirebaseMessagingPackage(),
        new RNFirebaseNotificationsPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    // AppEventsLogger.activateApp(this);

    // super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}

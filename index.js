/**
 * @format
 */
import * as React from 'react';
import {AppRegistry} from 'react-native';
import { Provider as PaperProvider, DefaultTheme, Colors } from 'react-native-paper';
import TabNavigator from './src/routes/tabNavigator';
import {name as appName} from './app.json';
// Redux
import { connect, Provider } from 'react-redux';
import { createStore } from 'redux';

import rootReducer from './redux/reducers';

// Nav
let store = createStore(rootReducer);

const theme = {
    ...DefaultTheme,
    roundness: 4,
    colors: {
      ...DefaultTheme.colors,
      primary: '#007dc6',
      accent: Colors.red500,
    }
};

export default function Main() {
    return (
        <PaperProvider theme={theme}>
            <Provider store={store}>
                <TabNavigator />
            </Provider>
        </PaperProvider>
    );
}

AppRegistry.registerComponent(appName, () => Main);

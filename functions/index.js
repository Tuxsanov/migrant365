'use strict';

const functions = require('firebase-functions');
const {Storage} = require('@google-cloud/storage');
const path = require('path');
const sharp = require('sharp');
const request = require('request');

var SMSru = require('sms_ru');

const THUMB_MAX_WIDTH = 200;
const THUMB_MAX_HEIGHT = 200;

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

/**
 * When an image is uploaded in the Storage bucket We generate a thumbnail automatically using
 * Sharp.
 */
exports.generateThumbnail = functions.storage.object().onFinalize(async (object) => {
    const fileBucket = object.bucket; // The Storage bucket that contains the file.
    const filePath = object.name; // File path in the bucket.
    const contentType = object.contentType; // File content type.
  
    // Exit if this is triggered on a file that is not an image.
    if (!contentType.startsWith('image/')) {
      console.log('This is not an image.');
      return null;
    }

    // Download file from bucket.
    const st = new Storage();
    const bucket = st.bucket(fileBucket);
  
    // Get the file name.
    const fileName = path.basename(filePath);
    // Exit if the image is already a thumbnail.
    if (fileName.startsWith('thumb_')) {
        try {
            const signedUrl = await bucket.file(filePath).getSignedUrl({action: "read", expires: "03-17-2025"});
            let _fileName = fileName.split('thumb_')[1];
            let _uid = _fileName.split('_')[0];
            await admin.firestore().doc(`user/${_uid}`).set({avatarImg: signedUrl[0]}, {merge: true})
            return;
        }catch(e){
            console.error(e);
            return;
        }
    }
    
    const metadata = {
        contentType: contentType,
    };
    // We add a 'thumb_' prefix to thumbnails file name. That's where we'll upload the thumbnail.
    const thumbFileName = `thumb_${fileName}`;
    const thumbFilePath = path.join(path.dirname(filePath), thumbFileName);
    // Create write stream for uploading thumbnail
    const thumbnailUploadStream = bucket.file(thumbFilePath).createWriteStream({metadata});
    
    // Create Sharp pipeline for resizing the image and use pipe to read from bucket read stream
    const pipeline = sharp();
    pipeline.resize(THUMB_MAX_WIDTH, THUMB_MAX_HEIGHT)
    // .max()
    .pipe(thumbnailUploadStream);
    
    bucket.file(filePath).createReadStream().pipe(pipeline);
    
    return new Promise(
        (resolve, reject) => thumbnailUploadStream.on('finish', resolve).on('error', reject)
    );
  
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.likePostUpdated = functions.firestore
    .document('posts/{postId}/likes/{uid}')
    .onUpdate((change, context) => {
        const dataAfter = change.after.data().liked;
        const dataBefore = change.before.data().liked;
        let updateData = {}

        if(dataAfter==0){
            updateData = {
                dislikes: admin.firestore.FieldValue.increment(1),
            }
            
            if(dataBefore==1){
                updateData = {
                    ...updateData,
                    likes: admin.firestore.FieldValue.increment(-1),
                }
            }
        }

        if(dataAfter==1){
            updateData = {
                likes: admin.firestore.FieldValue.increment(1)
            }

            if(dataBefore==0){
                updateData = {
                    ...updateData,
                    dislikes: admin.firestore.FieldValue.increment(-1),
                }
            }
        }

        if(dataAfter==-1){
            if(dataBefore==0){
                updateData = {
                    dislikes: admin.firestore.FieldValue.increment(-1),
                }
            }
    
            if(dataBefore==1){
                updateData = {
                    likes: admin.firestore.FieldValue.increment(-1)
                }
            }

        }

        const params = context.params;
        return admin.firestore().doc(`posts/${params.postId}`).update(updateData);
    });

exports.likePostCreated = functions.firestore
    .document('post_likes/{postId}/likes/{uid}')
    .onCreate((change, context) => {
        const data = change.data();
        const count = data.liked==1?1:-1;

        const params = context.params;
        return admin.firestore().doc(`posts/${params.postId}`).update({
            likes: admin.firestore.FieldValue.increment(count)
        });
    });

// exports.sendSMS = functions.auth.UserBuilder(

exports.sendSMS = functions.firestore
    .document('user/{uid}')
    .onUpdate((change, context) => {
        var sms = new SMSru('DC17941E-175A-75DD-0DE9-5664BE195F86 ');
        var phoneNumber = '+998997754262';
        let code = Math.floor(100000 + Math.random() * 900000);
        sms.sms_send({
            to: phoneNumber,
            text: code,
            from: 'migrant365.com',
        }, (e)=>{
            console.log('Sent successfully!');
        });

        return;

        // return admin.auth().getUserByPhoneNumber(phoneNumber).then(res=>{
        //     console.log(res.toJSON)
        // }, err=>console.log(err))
        // admin.auth().createUser({
        //     phoneNumber,
        //     password: 'code21',
        // }).then(console.log('created')).catch(err=>console.log(err));

        // return;

        // admin.auth()..getUser.updateUser(phoneNumber, {
        //     password: 
        // })
        // return;
        // const data = change.data();
        // const count = data.liked==1?1:-1;

        // const params = context.params;
        // return admin.firestore().doc(`posts/${params.postId}`).update({
        //     likes: admin.firestore.FieldValue.increment(count)
        // });
    });